'''Survive is an evolutionary simulator rgame in which you attempt to keep your group of animals alive.
The group can be altered by death and reproduction and reproduction can introduce mutations.
Eventually these mutations can lead to major changes in the population.
Copyright (C) 2013 Eric Butler'''

'''This file is part of Survive.

    Survive is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Survive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Survive.  If not, see <http://www.gnu.org/licenses/>.
    
The sound effects in this game are derived from several sources.  The original (and now heavily-modified)
sounds are credited as follows:
hits-kicks-punches-and-falls by Cosmic Embers www.youtube.com/cosmicembers
Wood Breaking by OTBTechno
Both sounds were retrieved from Freesound.org where they are licensed under the Creative Commons Attribution License.
Attributions are given as requested by the authors.  The use of these sounds does not indicate endorsement of this 
program by the creators of these sounds.'''

# make living food a bit smarter
# sprite blocks?
# there seems to be a navigation problem - some enemies are freezing, some food is not evading
# fast mode possible points is wrong
# saveable maps?

import pygame, sys, math, random, time
from pygame.locals import *
from evoSprites import *
import mutations, EvoFunctions, textwrap
from EvoFunctions import VertFrame,InsultOTron
from pgu import gui
pygame.init()

FPS = 30
fpsClock = pygame.time.Clock()

overviewFontBold = pygame.font.Font(None,48)
overviewFont = pygame.font.Font(None,42)
ThirtyTwoFont = pygame.font.Font(None,32)
ThirtySixFont = pygame.font.Font(None,36)
SeventyTwoFont = pygame.font.Font(None,72)

MODE = 'superfast' # might be 'slow', 'fast', or 'superfast'
REPROMODE = 'rand' # might also be 'even'
DIFFICULTY = 'hard' # might be 'pathetic','easy','normal','hard','everyonedies'

# sounds
singlehitsound = pygame.mixer.Sound('sounds/singlehit.wav')
doublehitsound = pygame.mixer.Sound('sounds/doublehit.wav')
hum = pygame.mixer.Sound('sounds/hum.wav')

# help pics
creaturehelppic = pygame.image.load('images/Creature help.png')
charhelppic = pygame.image.load('images/Characteristic help.png')
concepthelp = pygame.image.load('images/Concept help.png')
starthelp = pygame.image.load('images/Start help.png')

# standard pop up messages
FoodHit = '+1!'
LiveFoodHit = '+2!'
FailureHit = 'FAILURE!'
KillHit = 'FATALITY!'
InjuryHit = 'OUCH!'
StealHit = 'HEY! THAT\'S MINE!'
EscapeHit = 'SUCCESSFUL DEFENSE!'
ReverseInjury = 'CRITTER SMASH!'
# sets the amount of time to let you stare at your bloodstain after you die
DEADLAG = 1

app = gui.App()
app.connect(gui.QUIT,app.quit,None)

# standard colors
BLACK = (0,0,0)
WHITE = (255,255,255)
GREEN = (0,255,0)
BLUE = (0,0,255)
RED = (255,0,0)
YELLOW = (255,255,0)
GRAY = (146,146,146)
MAGENTA = (255,0,255)

# dictionary of history
BEGINNINGLIST = {}

def main():
    global SCREEN,controlBar,POINTS,TURN,BEGINNINGLIST,PREDSTR
    global PLAYERALIVE,critter,PREDATORLIST,THIEFLIST,LIVEFOODLIST,STARTPAUSE
    SCREEN.fill(WHITE)
    controlBar = pygame.Surface((deadArea,screenY))
    controlBar.fill(GRAY)
    EvoFunctions.UpdateControlBar = True

    POINTS= 0
    TURN = 1
    PLAYERALIVE = True
    PREDSTR = 4
    
    if MODE == 'fast':
        EnemySprite.liveCounterTime = (1,3)
    else:
        EnemySprite.liveCounterTime = (4,6)

    for critter in critterlist:
        critter.superkill()
    if MODE == 'fast':
        GM = []
        for i in range(0,3):
            (genes,message) = mutations.critterMut([10,3.5,.5,12,3,1,300,2,1])
            CritterSprite(genes[0],genes[1],genes[2],genes[3],genes[4],genes[5],genes[6],genes[7],
                                genes[8],mutations=message)
    elif MODE == 'slow':
        for i in range(0,2):
            CritterSprite(10,3.5,.5,12,3,1,300,4,1)
    else: # mode is superfast
        CritterSprite(10,3.5,.5,12,3,1,300,4,1,invincible=90)
        (genes,message) = mutations.critterMut([10,3.5,.5,12,3,1,300,2,1])
        CritterSprite(genes[0],genes[1],genes[2],genes[3],genes[4],genes[5],genes[6],genes[7],
                          genes[8],mutations=message,invincible=90)
    critter = critterlist[0]
    if DIFFICULTY == 'pathetic':
        predgenes = [10,.3,1.,350,18,0,'a']
        thiefgenes = [10,.3,1.,350,16,0,'a']
        lfgenes = [5,1,2,500,1,0]
    elif DIFFICULTY == 'easy':
        predgenes = [10,.5,1.5,400,19,0,'a']
        thiefgenes = [10,.5,1.5,400,17,0,'a']
        lfgenes = [5,1,2,500,5,1]
    elif DIFFICULTY == 'normal':
        predgenes = [11,.7,1.5,400,20,0,'a']
        thiefgenes = [11,.7,1.5,400,18,0,'a']
        lfgenes = [5,1,2,500,10,1]
    elif DIFFICULTY == 'hard':
        predgenes = [12,1.,1.5,425,20,0,'a']
        thiefgenes = [12,1.,1.5,425,18,0,'a']
        lfgenes = [6,1,2,500,10,1]
    elif DIFFICULTY == 'everyonedies':
        predgenes = [13,1.,1.5,450,20,0,'a']
        thiefgenes = [13,1.,1.5,450,18,0,'a']
        lfgenes = [6,1,2,500,10,1]
    if MODE == 'superfast':
        EvoFunctions.worldX = 4500
        EvoFunctions.worldY = 4500
        makefoodpos()
        critter.energy = 2
        PREDATORLIST = []
        for i in range(0,4): # Speed,Accel,Maneuver,Vision,Fight,Hide,Behave
            genes = mutations.otherMut(predgenes,'enemy')
            PREDATORLIST.append(EnemySprite('predator',genes))
        THIEFLIST = []
        for i in range(0,4):
            genes = mutations.otherMut(thiefgenes,'enemy')
            THIEFLIST.append(EnemySprite('thief',genes))
        LIVEFOODLIST = []
        for i in range(0,6):
            genes = mutations.otherMut([lfgenes[0],lfgenes[1],lfgenes[2],lfgenes[3],lfgenes[4],lfgenes[5],['run','hide'][random.randint(0,1)]],'food')
            LIVEFOODLIST.append(LiveFoodSprite(genes))
    else:
        EvoFunctions.worldX = 3000
        EvoFunctions.worldY = 3000
        PREDATORLIST = []
        for i in range(0,6): # Speed,Accel,Maneuver,Vision,Fight,Hide,Behave
            genes = mutations.otherMut(predgenes,'enemy')
            PREDATORLIST.append(EnemySprite('predator',genes))
        THIEFLIST = []
        for i in range(0,6):
            genes = mutations.otherMut(thiefgenes,'enemy')
            THIEFLIST.append(EnemySprite('thief',genes))
        LIVEFOODLIST = []
        for i in range(0,12):
            genes = mutations.otherMut([lfgenes[0],lfgenes[1],lfgenes[2],lfgenes[3],lfgenes[4],lfgenes[5],['run','hide'][random.randint(0,1)]],'food')
            LIVEFOODLIST.append(LiveFoodSprite(genes))
        
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,'hide':0}
    BEGINNINGLIST['livefood'] = EvoFunctions.otherAvg(LIVEFOODLIST,averages)
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,'hide':0}
    BEGINNINGLIST['critters'] = EvoFunctions.otherAvg(critterlist,averages)
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,}
    BEGINNINGLIST['preds'] = EvoFunctions.otherAvg(PREDATORLIST,averages)
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,}
    BEGINNINGLIST['kleptos'] = EvoFunctions.otherAvg(THIEFLIST,averages)
        
    STARTPAUSE = True
    SCREEN.fill(BLACK)
    starthelpimage = gui.Image(starthelp)
    donebutton = gui.Button('OK',font=overviewFont)
    donebutton.connect(gui.CLICK,unstartpause)
    frame = VertFrame()
    frame.add(starthelpimage)
    frame.add(donebutton)
    app.init(frame.container)
    app.paint()
    pygame.display.flip()
    
    while STARTPAUSE == True:
        events = pygame.event.get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_RETURN:
                    STARTPAUSE = False
            app.event(event)
    makeMap()
    
def unstartpause():
    global STARTPAUSE
    STARTPAUSE = False

def makeMap():
    global fogMap,fog,critter,LabelList,run,PLAYERALIVE,LOCKESCAPE,TEMPMESSAGE,camoPercent
    global PREDATORLIST,THIEFLIST,LIVEFOODLIST,RETURNEVENT,ACTIVESCREEN,app,GENCOUNTER,purplehaze
    global FOODOS,LIVEFOODOS,THIEFOS,PREDOS

    clear_all_widgets()

    run = False
    PLAYERALIVE = True
    LOCKESCAPE = True
    TEMPMESSAGE = False # this says that there is a message on the screen to be cleared with the Enter key
    RETURNEVENT = False # event to call when ENTER is pressed
    if MODE == 'superfast':
        GENCOUNTER = 100
    
    # clear out the sprite groups in case we are restarting
    for x in SpriteGroups:
        x.empty()
    LabelList = []
    
    # make fog of war
    fogMap = pygame.Surface((EvoFunctions.worldX+10,EvoFunctions.worldY+10))
    fogMap.fill((0,0,0))
    fogMap.set_colorkey(WHITE)
    fog = pygame.Surface((screenX,screenY))
    fog.fill((0,0,0))
    fog.set_colorkey(WHITE)
    fog.set_alpha(50)
    
    if MODE == 'superfast':
        purplehaze = pygame.Surface((screenX,screenY))
        purplehaze.fill(MAGENTA)
        purplehaze.set_alpha(50)

    # regenerate enemies and live food
    if MODE == 'superfast':
        while len(LIVEFOODLIST) < 6:
            genes = mutations.pickFood(LIVEFOODLIST)
            LIVEFOODLIST.append(LiveFoodSprite(genes))
        while len(PREDATORLIST) < 5:
            genes = mutations.pickEnemy(PREDATORLIST)
            PREDATORLIST.append(EnemySprite('predator',genes))
        while len(THIEFLIST) < 5:
            genes = mutations.pickEnemy(THIEFLIST)
            THIEFLIST.append(EnemySprite('thief',genes))
    else:
        while len(LIVEFOODLIST) < 12:
            genes = mutations.pickFood(LIVEFOODLIST)
            LIVEFOODLIST.append(LiveFoodSprite(genes))
        while len(PREDATORLIST) < 6:
            genes = mutations.pickEnemy(PREDATORLIST)
            PREDATORLIST.append(EnemySprite('predator',genes))
        while len(THIEFLIST) < 6:
            genes = mutations.pickEnemy(THIEFLIST)
            THIEFLIST.append(EnemySprite('thief',genes))    

    # add food, enemy, and terrain objects
    FOODOS = 0 # food on screen
    if MODE == 'fast':
        maxfood = random.randint(5,10)
    elif MODE == 'superfast':
        maxfood = 0
    else:
        maxfood = random.randint(3,8)
    for i in range(0,maxfood):
        food = FoodSprite()
        foods.add(food)
        allOtherSprites.add(food)
        FOODOS+=1
    lfood = LIVEFOODLIST[:]
    LIVEFOODOS = 0 # livefood on screen
    if MODE == 'fast':
        maxlivefood = random.randint(3,5)
    elif MODE == 'superfast':
        maxlivefood = 5
    else:
        maxlivefood = random.randint(1,3)
    for i in range(0,maxlivefood):
        food = lfood.pop(random.randint(0,len(lfood)-1))
        food.place()
        foods.add(food)
        livefoods.add(food)
        AllSprites.add(food) # these guys are drawn as movers not food
        LIVEFOODOS+=1
    playerSprite.add(critter)
    if MODE != 'superfast':
        critter.usedThisRound = True
    critter.x = EvoFunctions.worldX/2
    critter.y = EvoFunctions.worldY/2
    burrow = None
    if MODE != 'superfast':
        burrow = TerrainSprite(holepic,EvoFunctions.worldX/2,EvoFunctions.worldY/2)
        allOtherSprites.add(burrow)
        tp = TerrainPick(160)
    else:
        tp = TerrainPick(80)
    for i in tp:
        terrainblob = TerrainSprite(x=i[0],y=i[1])
        allOtherSprites.add(terrainblob)
    if MODE == 'superfast':
        for i in range(0,random.randint(6,9)):
            oddblob = InterestingTerrainPatch()
    else:
        for i in range(0,random.randint(3,7)):
            oddblob = InterestingTerrainPatch()
    PREDOS = 0
    THIEFOS = 0
    if MODE == 'fast':
        maxpreds = random.randint(1,len(critterlist))
        maxthieves = random.randint(1,len(critterlist))
    elif MODE == 'superfast':
        maxpreds = 3
        maxthieves = 3
    else:
        maxpreds = random.randint(1,len(critterlist))
        maxthieves = random.randint(1,len(critterlist))
    preds = PREDATORLIST[:]
    thieves = THIEFLIST[:]
    for i in range(0,maxpreds):
        enemy = preds.pop(random.randint(0,len(preds)-1))
        enemy.place()
        enemies.add(enemy)
        PREDOS+=1
    for i in range(0,maxthieves):
        enemy = thieves.pop(random.randint(0,len(thieves)-1))
        enemy.place()
        enemies.add(enemy)
        THIEFOS+=1
    AllSprites.add(enemies)
    AllSprites.add(critter)
    AllSprites.add(allOtherSprites)

    # draw a black canvas and the control bar
    SCREEN.fill(BLACK)
    controlbartext()
    SCREEN.blit(controlBar,(screenX,0))
    camoPercent = TextSprite('100% camouflage',BLUE,screenX+20,460)
    textGroup.add(camoPercent)
    textGroup.draw(SCREEN)
    pygame.display.flip()
    runMessage(critter,playerSprite,foods,burrow,allOtherSprites)

def runMessage(critter,playerSprite,foods,burrow,allOtherSprites):
    global run
    message = gui.Table()
    if MODE == 'superfast':
        if critter.usedThisRound:
            text = 'Returning to previous critter'
        else:
            text = 'New critter'
        critter.usedThisRound = True
        label = gui.Label(text)
        label.set_font(ThirtyTwoFont)
        if critter.ancestor != False:
            text1 = 'Child of '+str(critter.ancestor)
            label1 = gui.Label(text1)
            label1.set_font(ThirtyTwoFont)
        else:
            text1 = '(No ancestor)'
            label1 = gui.Label(text1)
            label1.set_font(ThirtyTwoFont)
        message.td(label,0,0,background=(200,200,200))
        message.td(label1,0,1,background=(200,200,200))
        step=2
        for mutation in critter.mutations:
            label = gui.Label(mutation,color=BLUE)
            label.set_font(ThirtyTwoFont)
            message.td(label,0,step,background=(200,200,200))
            step+=1
    else: # MODE is not superfast
        text1 = ' '+critter.name+' has '+str(critter.energy)+' energy'
        text= ' and needs '
        if critter.energy < critter.foodNeed and MODE != 'fast':
            text+=str(critter.foodNeed-critter.energy)+' energy this round to survive '
            label1 = gui.Label(text1)
            label1.set_font(ThirtyTwoFont)
            label = gui.Label(text)
            label.set_font(ThirtyTwoFont)
            message.td(label1,0,0,background=(200,200,200))
            message.td(label,0,1,background=(200,200,200))
            step = 2
        elif MODE != 'fast':
            text = ' Ready to move your next critter? '
            label = gui.Label(text)
            label.set_font(ThirtyTwoFont)
            message.td(label,0,0,background=(200,200,200))
            step = 1
        if MODE == 'fast':
            text = ' '+str(critter.name)+' needs '+str(critter.foodNeed+critter.reproCost)+' energy '
            label1 = gui.Label(text)
            label1.set_font(ThirtyTwoFont)
            message.td(label1,0,0,background=(200,200,200))
            text = ' to replace itself next turn '
            label = gui.Label(text)
            label.set_font(ThirtyTwoFont)
            message.td(label,0,1,background=(200,200,200))
            step = 2
            if TURN == 1 and len(critter.mutations) > 0:
                label = gui.Label('Mutations:',color=BLUE)
                label.set_font(ThirtyTwoFont)
                message.td(label,0,step,background=(200,200,200))
                step+=1
                for mutation in critter.mutations:
                    label = gui.Label(mutation,color=BLUE)
                    label.set_font(ThirtyTwoFont)
                    message.td(label,0,step,background=(200,200,200))
                    step+=1
    button = gui.Button('Continue',width=100,height=30,font=ThirtySixFont)
    button.connect(gui.CLICK,runNow)
    message.td(button,0,step,background=(200,200,200))
    runloop(critter,playerSprite,foods,burrow,allOtherSprites,message)

def runNow():
    global run,LOCKESCAPE,TEMPMESSAGE
    clear_all_widgets()
    run = True
    LOCKESCAPE = False
    TEMPMESSAGE = False

def runloop(critter,playerSprite,foods,burrow,allOtherSprites,message):
    global run,eaten,cameraX,cameraY,fogMap,CRITTERSTEP,TEMPMESSAGE,RETURNEVENT,PLAYERALIVE,GENCOUNTER
    global FOODOS,LIVEFOODOS,POINTS
    clear_all_widgets()
    offburrow = False
    TEMPMESSAGE = True
    endRoundFlag = False
    app.init(message)
    app.paint()
    pygame.display.flip()
    while True:
        # empty delta values used to change direction
        headingdelta = 0
        speeddelta = 0

        events = pygame.event.get()
        for event in events:
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE and LOCKESCAPE == False:
                    if run == True:
                        run = False
                        clear_all_widgets()
                        makePauseAlert()
                    elif run == False:
                        run = True
                        clear_all_widgets()
                elif event.key == K_RETURN and TEMPMESSAGE == True:
                    runNow()
                elif event.key == K_RETURN and RETURNEVENT != False:
                    RETURNEVENT()
                    RETURNEVENT = False
                elif event.key == K_k and LOCKESCAPE == False:
                    critter.die()
                    PLAYERALIVE = False
            app.event(event)
        if run == True: # game not paused
            pygame.mouse.set_visible(False)
            # detect keyboard input
            if pygame.key.get_pressed()[pygame.K_LEFT]:
                headingdelta=1
            if pygame.key.get_pressed()[pygame.K_RIGHT]:
                headingdelta=-1
            if pygame.key.get_pressed()[pygame.K_UP]:
                speeddelta=1
            if pygame.key.get_pressed()[K_DOWN]:
                speeddelta=-1
            # clear hits so they only appear for a single frame
            hits.empty()
            # in superfast mode cause energy to drop, update counter
            flarescreen = False
            if MODE == 'superfast' and critter.alive:
                flarescreen = superfastLoop()
            # adjust critter, enemy, and live food heading and speed
            critter.move(headingdelta,speeddelta)
            for enemy in enemies:
                if enemy.hit == False:
                    EvoFunctions.Follow(enemy,critter)
                enemy.walk()
                # this means that enemies are unhidden/unslowed unless they are colliding with terrain
                enemy.slowed = False
                enemy.hidden = False
            for food in livefoods:
                EvoFunctions.escape(critter,food)
            #find collisions
            if critter.alive == True:
                food_collisions = pygame.sprite.spritecollide(critter,foods,False,pygame.sprite.collide_circle)
                enemy_collisions = pygame.sprite.spritecollide(critter,enemies,False,pygame.sprite.collide_circle)
                odd_collisions = pygame.sprite.spritecollide(critter,oddTerrainSprites,False,pygame.sprite.collide_circle)
                for food in food_collisions:
                    if food.invincible == False:
                        (x,y) = EvoFunctions.findHitMarker(critter,food)
                        hit = HitSprite(eatpic,x,y)
                        hits.add(hit)
                        AllSprites.add(hit)
                        if food.name == 'food':
                            singlehitsound.play()
                            popup = PopUpSprite(FoodHit,GREEN,x,y)
                            EvoFunctions.eatFood(critter,food) # set labels and critter to update
                            food.superkill()
                            FOODOS-=1
                        elif food.name == 'live':
                            (message,color)=fight(critter,food)
                            popup = PopUpSprite(message,color,x,y)
                            if message not in (FailureHit,InjuryHit,KillHit):
                                EvoFunctions.eatFood(critter,food) # set labels and critter to update
                                food.kill()
                                LIVEFOODOS-=1
                            else:
                                food.invincible = time.clock()
                                BigBloodSplatter(x,y,(2,5),(40,50))
                        AllSprites.add(popup)
                        popUps.add(popup)
                critter.hidden = False
                for odd in odd_collisions:
                    if odd.name == 'hider':
                        critter.hidden = True
                if offburrow == True and critter.invincible <= 0:
                    for enemy in enemy_collisions:
                        if enemy.hit == False:
                            if enemy.name == 'thief' and len(critter.carry) < 1:
                                pass
                            else:
                                (x,y) = EvoFunctions.findHitMarker(critter,enemy)
                                hit = HitSprite(damagepic,x,y)
                                hits.add(hit)
                                AllSprites.add(hit)
                                (message,mColor) = fight(critter,enemy)
                                popup = PopUpSprite(message,mColor,x,y)
                                popUps.add(popup)
                                AllSprites.add(popup)
                                if message == KillHit:
                                    BigBloodSplatter(x,y,(10,15),(50,150))
                                elif message == StealHit:
                                    pass
                                else:
                                    BigBloodSplatter(x,y,(2,5),(40,50))
            # collisions between other creatures and terrain
            odd_collisions_enemies = pygame.sprite.groupcollide(enemies,oddTerrainSprites,False,False,pygame.sprite.collide_circle)
            odd_collisions_food = pygame.sprite.groupcollide(foods,oddTerrainSprites,False,False,pygame.sprite.collide_circle)
            for enemy in odd_collisions_enemies:
                enemy_odd_collisions = pygame.sprite.spritecollide(enemy,oddTerrainSprites,False,pygame.sprite.collide_circle)
                slowed = False
                hidden = False
                for oddball in enemy_odd_collisions:
                    if oddball.name == 'hider':
                        hidden = True
                    elif oddball.name == 'slowdown' and enemy.slowed == False:
                        slowed = True
                enemy.hidden = hidden
                enemy.slowed = slowed
            for food in odd_collisions_food:
                if food.name == 'live':
                    food_odd_collisions = pygame.sprite.spritecollide(food,oddTerrainSprites,False,pygame.sprite.collide_circle)
                    hidden = False
                    for oddball in food_odd_collisions:
                        if oddball.name == 'hider':
                            hidden = True
                    food.hidden = hidden
            # check for bleeding and add bloodsplatter
            if critter.bleeding != False:
                if (time.time()-critter.bleeding) > .1:
                    (x,y) = (critter.x+random.randint(0,15),critter.y+random.randint(0,15))
                    trail = BloodTrailSprite(x,y,'bloodtrail')
                    AllSprites.add(trail)
                    bloodtrails.add(trail)
                    critter.bleeding = time.time()+random.uniform(-.1,.1)
                if (time.time()-critter.initialBleeding) > 3:
                    critter.bleeding = False
                    critter.initialBleeding = False
            for enemy in enemies:
                if enemy.bleeding != False:
                    if (time.time()-enemy.bleeding) > .1:
                        (x,y) = (enemy.x+random.randint(0,15),enemy.y+random.randint(0,15))
                        trail = BloodTrailSprite(x,y,'bloodtrail')
                        AllSprites.add(trail)
                        bloodtrails.add(trail)
                        enemy.bleeding = time.time()+random.uniform(-.1,.1)
                if (time.time()-enemy.initialBleeding) > 3:
                    enemy.bleeding = False
                    enemy.initialBleeding = False
            for trail in bloodtrails:
                trail.counter-=1
                if trail.counter <= 0:
                    trail.kill()
            # set camera and stop it so that world edge is screen edge
            # camera position is effectively the top left corner of visible screen
            cameraX = critter.x-(screenX/2)
            cameraY = critter.y-(screenY/2)
            if cameraX > (EvoFunctions.worldX-(screenX)):
                cameraX = (EvoFunctions.worldX-(screenX))
            elif cameraX < -10:
                cameraX = -10
            if cameraY > (EvoFunctions.worldY-(screenY)):
                cameraY = (EvoFunctions.worldY-(screenY))
            elif cameraY < -10:
                cameraY = -10
            for sprite in AllSprites:
                sprite.cameraCoords(cameraX,cameraY)
            # reset hit if enemy is more than 1000 pixels away
            for enemy in enemies:
                if enemy.hit == True:
                    if (((enemy.rect.x-critter.rect.x)**2)+((enemy.rect.y-critter.rect.y)**2)) > (500**2):
                        enemy.hit = False
            # check for end of round - must happen after camera adjustments or new critters start not on the burrow
            if MODE != 'superfast':
                if pygame.sprite.collide_circle(critter,burrow): # critter is in/on burrow
                    if offburrow == True:
                        endRoundFlag = True
                else:
                    offburrow = True
            else:
                offburrow = True
            # create fog of war
            create_fog(critter)
            # find visible moving objects
            # first clear all movers
            movers.empty()
            # now add visible movers back in
            for enemy in enemies:
                if (critter.vision**2) >= (((enemy.x-critter.x)**2)+((enemy.y-critter.y)**2)):
                    movers.add(enemy)
            for food in livefoods:
                canSee = EvoFunctions.isVisible(food,critter,maxSpeed=5)
                if canSee == True:
                    movers.add(food)
            # update controlbar
            if EvoFunctions.UpdateControlBar == True:
                controlbartext()
            # update camo label
            speed = critter.speed
            if speed <= 0:
                percent = 100
            else:
                speed = critter.speed
                if speed > 9.:
                    percent = 10
                else:
                    percent = int((1-(speed/10))*100)
            camoPercent.update(text=str(percent)+'% camouflaged')
            # draw everybody
            SCREEN.fill(WHITE)
            allOtherSprites.draw(SCREEN)
            for x in bloodtrails:
                x.draw()
            movers.draw(SCREEN)
            playerSprite.draw(SCREEN)
            SCREEN.blit(fogMap,(-(cameraX+10),-(cameraY+10)))
            SCREEN.blit(fog,(0,0))
            hits.draw(SCREEN)
            popUps.draw(SCREEN)
            SCREEN.blit(controlBar,(screenX,0))
            textGroup.draw(SCREEN)
            if flarescreen:
                SCREEN.blit(purplehaze,(0,0))
                flare = SimpleSprite(purpleflare)
                SCREEN.blit(flare.image,(-40,-40))
            pygame.display.flip()
            if PLAYERALIVE != True: # player is dead
                if (time.clock() - PLAYERALIVE) > DEADLAG:
                    run = False # pause after death
                    if MODE == 'superfast':
                        POINTS-=10
                        if critter.energy <= 0: # critter starved
                            newSuperfastRound(starved=True)
                        else:
                            newSuperfastRound()
                    else:
                        clear_all_widgets()
                        makeDeadMessage('kill',[critter.name])
            if endRoundFlag: # put this AFTER the draw loop so it doesn't get drawn over
                critterNext()
            app.paint()

        else: # game paused
            pygame.mouse.set_visible(True)
        
        fpsClock.tick(FPS)

def superfastLoop():
    global GENCOUNTER,FOODOS,LIVEFOODOS,PREDOS,THIEFOS,PLAYERALIVE,POINTS
    global PREDSTR
    flarescreen = False
    if DIFFICULTY == 'pathetic' or DIFFICULTY == 'easy':
        fooddecrease = .0017 # rate at which food is used up by critter
    else:
        fooddecrease = .0025 # rate at which food is used up by critter
    critter.energy-=critter.foodNeed*fooddecrease
    if critter.energy < 0: # starved
        critter.energy = 0
        critter.alive = False
        critter.max_speed = 0
        critter.accel = 0
        critter.maneuver = 0
        critter.baseimage = critterstarved
        PLAYERALIVE = time.clock()
    fooddel = False
    if len(critter.carry) > 0: # remove used up food from carrying
        food = critter.carry[0]
        food.value-=fooddecrease
        if food.value <= 0.3:
            fooddel = True
    if fooddel:
        fooddel = False
        critter.carry.pop(0)
        EvoFunctions.updateCarry(critter)
    if critter.invincible > 0:
        critter.invincible-=1
    GENCOUNTER-=(.03*critter.energy)/critter.reproCost # count down to the next generation
    if GENCOUNTER < 0:
        GENCOUNTER = 100
        PREDSTR+=1
        if PREDSTR > 8:
            PREDSTR = 8
        flarescreen = True
        hum.play()
        POINTS+=10
        genes = [critter.base_max_speed,critter.base_maneuver,
            critter.base_accel,critter.base_fight,critter.base_strength,
            critter.hide,critter.vision,critter.reproCost,critter.foodNeed]
        (reprocritgenes,message) = mutations.critterMut(genes)
        ancestor = critter.name
        newcrit = CritterSprite(reprocritgenes[0],reprocritgenes[1],reprocritgenes[2],reprocritgenes[3],reprocritgenes[4],
            reprocritgenes[5],reprocritgenes[6],reprocritgenes[7],reprocritgenes[8],ancestor=ancestor,mutations=message,
            invincible=90)
        newSuperfastRound(kill=False) # change critter
    COUNTERBAR.update((100-GENCOUNTER),MAGENTA)
    EvoFunctions.UpdateControlBar = True
    
    # reduce energy of enemies
    for enemy in enemies:
        enemy.energyCounter-=.5
        if enemy.energyCounter <=0 and enemy.detecting == False: # don't starve an enemy that is actually chasing you
            body = BloodTrailSprite(enemy.x,enemy.y,enemystarved)
            body.image = pygame.transform.rotate(body.image,enemy.heading)
            notdiscovered.add(body)
            AllSprites.add(body)
            enemy.kill()
            if enemy.name == 'predator':
                PREDOS-=1
            elif enemy.name == 'thief':
                THIEFOS-=1
    
    # figure out whether food/bodies have been seen, add them to drawn methods
    for item in notdiscovered:
        if item.name == 'blood': # this is a body, iterate counter so it fades
            item.counter-=1
        # can critter see the item?
        if (critter.vision**2) >= (((critter.x-item.x)**2)+((critter.y-item.y)**2)):
            if item.name == 'blood':
                bloodtrails.add(item)
            else:
                allOtherSprites.add(item)
            notdiscovered.remove(item)
    
    # replenish food
    while FOODOS < 10:
        food = FoodSprite(dist=critter.vision+50,coords=(critter.x,critter.y))
        food.place()
        foods.add(food)
        notdiscovered.add(food)
        AllSprites.add(food)
        FOODOS+=1
    
    # replenish live food
    while LIVEFOODOS < 5:
        genes = mutations.pickFood(LIVEFOODLIST)
        food = LiveFoodSprite(genes,dist=critter.vision+50,coords=(critter.x,critter.y))
        LIVEFOODLIST.append(food)
        food.place(coords=(critter.x,critter.y))
        foods.add(food)
        livefoods.add(food)
        AllSprites.add(food) # these guys are drawn as movers not food
        LIVEFOODOS+=1
        
    # regenerate enemies
    while PREDOS < PREDSTR:
        genes = mutations.pickEnemy(PREDATORLIST)
        enemy = EnemySprite('predator',genes,dist=critter.vision+50,coords=(critter.x,critter.y))
        PREDATORLIST.append(enemy)
        enemy.place(coords=(critter.x,critter.y))
        enemies.add(enemy)
        AllSprites.add(enemy)
        PREDOS+=1
    while THIEFOS < PREDSTR:
        genes = mutations.pickEnemy(THIEFLIST)
        enemy = EnemySprite('thief',genes,dist=critter.vision+50,coords=(critter.x,critter.y))
        THIEFLIST.append(enemy)
        enemy.place(coords=(critter.x,critter.y))
        enemies.add(enemy)
        AllSprites.add(enemy)
        THIEFOS+=1
    # kill off extra enemies
    if PREDSTR < PREDOS or PREDSTR < THIEFOS:
        minPlife = 9999
        minTlife = 9999
        minP = None
        minT = None
        for pred in enemies:
            if pred.name == 'predator':
                if pred.energyCounter < minPlife:
                    minPlife = pred.energyCounter
                    minP = pred
            elif pred.name == 'thief':
                if pred.energyCounter < minTlife:
                    minTlife = pred.energyCounter
                    minT = pred
        if PREDSTR < PREDOS: # set hungriest preds and thieves to starve immediately
            minP.energyCounter = -1
        if PREDSTR < THIEFOS:
            minT.energycounter = -1
        
    # heal injuries
    if critter.alive:
        for [base,atr] in EvoFunctions.heallist:
            if critter.__dict__[atr] < critter.__dict__[base] and critter.energy > critter.foodNeed:
                EvoFunctions.UpdateControlBar = True
                critter.__dict__[atr]+=.0016*critter.__dict__[base]
                critter.energy-=.0016*critter.foodNeed
                if len(critter.carry) > 0: # use of 'weight' of food that's being consumed
                    food = critter.carry[0]
                    food.value-=.0016*critter.foodNeed
                    EvoFunctions.updateCarry(critter)
                if critter.__dict__[atr] > critter.__dict__[base]:
                    critter.__dict__[atr] = critter.__dict__[base]
        if critter.base_speed_no_carry < critter.base_max_speed and critter.energy > critter.foodNeed:
            EvoFunctions.UpdateControlBar = True
            critter.base_speed_no_carry+=.0016*critter.base_max_speed
            critter.energy-=.0016*critter.foodNeed
            if critter.base_speed_no_carry > critter.base_max_speed:
                critter.base_speed_no_carry = critter.base_max_speed
            EvoFunctions.updateCarry(critter)
    
    return(flarescreen)

def newSuperfastRound(starved=False,kill=True):
    global POINTS,critter,run,PLAYERALIVE,GENCOUNTER
    run = False
    for enemy in enemies:
        enemy.place()
    for food in foods:
        if food.name == 'food':
            food.superkill()
    (x,y) = (critter.x,critter.y)
    heading = critter.heading
    name = critter.name
    playerSprite.empty()
    AllSprites.remove(critter)
    if kill:
        critter.superkill()
    if len(allcritterlist) > 0:
        critter = allcritterlist[-1]
        if critter.energy == 0:
            critter.energy = 2
        (critter.x,critter.y) = (x,y)
        (critter.rect.centerx,critter.rect.centery) = (critter.x-cameraX,critter.y-cameraY)
        critter.heading = heading
        AllSprites.add(critter)
        playerSprite.add(critter)
        PLAYERALIVE = True
        GENCOUNTER = 100
        if starved:
            makeDeadMessage('starve',[name])
        else:
            runMessage(critter,playerSprite,foods,None,allOtherSprites)
    else:
        makeAllDeadAlert()

def newFastRound():
    global POINTS,run,critter,PREDATORLIST,THIEFLIST
    run = False
    for crit in allcritterlist:
        if crit.alive == True:
            POINTS+=1
        crit.carry = []
        EvoFunctions.updateCarry(crit)
        crit.energy-=crit.foodNeed
        if crit.energy < 0:
            crit.die()
        # heal ALL injuries before you repro and die
        while crit.fight < crit.base_fight and crit.energy > 0:
            crit.energy-=(crit.foodNeed/2.0)
            crit.fight+=(crit.base_fight/2.0)
        while crit.accel < crit.base_accel and crit.energy > 0:
            crit.energy-=(crit.foodNeed/2.0)
            crit.accel+=(crit.base_accel/2.0)
        while crit.max_speed < crit.base_max_speed and crit.energy > 0:
            crit.energy-=(crit.foodNeed/2.0)
            crit.max_speed+=(crit.base_max_speed/2.0)
            crit.base_speed_no_carry+=(crit.base_max_speed/2.0)
        while crit.maneuver < crit.base_maneuver and crit.energy > 0:
            crit.energy-=(crit.foodNeed/2.0)
            crit.maneuver+=(crit.base_maneuver/2.0)
        while crit.strength < crit.base_strength and crit.energy > 0:
            crit.energy-=(crit.foodNeed/2.0)
            crit.strength+=(crit.base_strength/2.0)
    # makes new critter list, kill old ones
    newnum = 0
    if REPROMODE == 'rand':
        critpicklist = []
    elif REPROMODE == 'even':
        critpicklist = {0:[],1:[],2:[],3:[]}
    step = 0
    for crit in allcritterlist:
        if crit.alive == True and crit.energy > 0:
            rangenum = int(crit.energy/crit.reproCost)
            if REPROMODE == 'rand':
                for x in range(0,rangenum):
                    newnum+=1
                    critpicklist.append(([crit.base_max_speed,crit.base_maneuver,
                        crit.base_accel,crit.base_fight,crit.base_strength,
                        crit.hide,crit.vision,crit.reproCost,crit.foodNeed],crit.name))
            elif REPROMODE == 'even':
                for x in range(0,rangenum):
                    newnum+=1
                    critpicklist[step].append(([crit.base_max_speed,crit.base_maneuver,
                        crit.base_accel,crit.base_fight,crit.base_strength,
                        crit.hide,crit.vision,crit.reproCost,crit.foodNeed],crit.name))
        step+=1
    allcritterlist[:] = []
    critterlist[:] = []
    usednames[:] = []
    deadlist[:] = []
    # makes new critters
    done = False
    if len(critpicklist) <= 0:
        done = True
    mutationList = []
    if REPROMODE == 'even': # figure out which critters to pick from
        pickables = []
        for x in range(0,4):
            if len(critpicklist[x]) > 0:
                pickables.append(x)
    step = 0
    while done == False:
        if REPROMODE == 'rand':
            pick = critpicklist.pop(random.randint(0,len(critpicklist)-1))
            newnum-=1
        elif REPROMODE == 'even': # first get one offspring from everyone, then pick others at random
            if step < len(pickables):
                pick = critpicklist[pickables[step]].pop(random.randint(0,len(critpicklist[pickables[step]])-1))
                step+=1
            else:
                pickmade = False
                while pickmade == False:
                    try:
                        pick = critpicklist[pickables[random.randint(0,len(pickables)-1)]].pop(random.randint(0,len(critpicklist)-1))
                        pickmade = True
                    except ValueError:
                        pass
            newnum-=1
        (reprocritgenes,message) = mutations.critterMut(pick[0])
        ancestor = pick[1]
        CritterSprite(reprocritgenes[0],reprocritgenes[1],reprocritgenes[2],reprocritgenes[3],reprocritgenes[4],
               reprocritgenes[5],reprocritgenes[6],reprocritgenes[7],reprocritgenes[8],ancestor=ancestor,mutations=message)
        if newnum <= 0 or len(critterlist) >= 4: # if there is nothing in picklist or no spaces for new critters
            done = True
    # clean up predators, etc
    for enemy in PREDATORLIST:
        enemy.liveCounter-=1
        if enemy.liveCounter <= 0:
            enemy.kill()
            PREDATORLIST.remove(enemy)
    for enemy in THIEFLIST:
        enemy.liveCounter-=1
        if enemy.liveCounter <= 0:
            enemy.kill()
            THIEFLIST.remove(enemy)
    try:
        critter = critterlist[0]
        overviewFunc(repro=True,showmuts=True)
    except IndexError:
        clear_all_widgets()
        makeAllDeadAlert()

def newRound():
    global TURN,POINTS,run,critter,PREDATORLIST,THIEFLIST
    TURN+=1
    if MODE == 'fast':
        newFastRound()
    else:
        for crit in allcritterlist:
            if crit.alive == True:
                POINTS+=1
            crit.usedThisRound = False
            crit.speed = 0
            crit.heading = 180
            crit.carry = []
            EvoFunctions.updateCarry(crit)
            crit.energy-=crit.foodNeed
            if crit.energy < 0:
                if crit.alive == True:
                    CritterSprite.starved.append(crit.name)
                crit.die()
                POINTS-=1
            # heal injuries
            if crit.fight < crit.base_fight and crit.energy >= (crit.foodNeed/2.0):
                crit.energy-=(crit.foodNeed/2.0)
                crit.fight+=(crit.base_fight/2.0)
                if crit.fight > crit.base_fight:
                    crit.fight = crit.base_fight
            if crit.accel < crit.base_accel and crit.energy >= (crit.foodNeed/2.0):
                crit.energy-=(crit.foodNeed/2.0)
                crit.accel+=(crit.base_accel/2.0)
                if crit.accel > crit.base_accel:
                    crit.accel = crit.base_accel
            if crit.max_speed < crit.base_max_speed and crit.energy >= (crit.foodNeed/2.0):
                crit.energy-=(crit.foodNeed/2.0)
                crit.max_speed+=(crit.base_max_speed/2.0)
                crit.base_speed_no_carry+=(crit.base_max_speed/2.0)
                if crit.max_speed > crit.base_max_speed:
                    crit.max_speed = crit.base_max_speed
                if crit.base_speed_no_carry > crit.base_max_speed:
                    crit.base_speed_no_carry = crit.base_max_speed
            if crit.maneuver < crit.base_maneuver and crit.energy >= (crit.foodNeed/2.0):
                crit.energy-=(crit.foodNeed/2.0)
                crit.maneuver+=(crit.base_maneuver/2.0)
                if crit.maneuver > crit.base_maneuver:
                    crit.maneuver = crit.base_maneuver
            if crit.strength < crit.base_strength and crit.energy >= (crit.foodNeed/2.0):
                crit.energy-=(crit.foodNeed/2.0)
                crit.strength+=(crit.base_strength/2.0)
                if crit.strength > crit.base_strength:
                    crit.strength = crit.base_strength
        for crit in allcritterlist:
            if crit.alive == False:
                crit.die()
        for dead in deadlist:
            dead.superkill()
        for enemy in PREDATORLIST:
            enemy.liveCounter-=1
            if enemy.liveCounter <= 0:
                enemy.kill()
                PREDATORLIST.remove(enemy)
        for enemy in THIEFLIST:
            enemy.liveCounter-=1
            if enemy.liveCounter <= 0:
                enemy.kill()
                THIEFLIST.remove(enemy)
        try:
            critter = critterlist[0]
            overviewFunc(repro=True)
        except IndexError:
            run = False
            clear_all_widgets()
            makeAllDeadAlert()

# determines what to do when a critter dies or returns to the burrow
def critterNext():
    global critter
    clear_all_widgets()
    notUsed = []
    for crit in critterlist:
        if crit.usedThisRound == False:
            notUsed.append(crit)
    if len(notUsed) > 0: # there are more critters to be used
        critter = notUsed[0]
        makeMap()
    else: # all critters have been used
        newRound()

# allows one to change the settings
def settingsFunc():
    global LOCKESCAPE
    # do NOT allow the user to change things and then hit escape to exit!
    LOCKESCAPE = True
    clear_all_widgets()
    table = gui.Table()
    modegroup = gui.Group(name='modes',value=MODE)
    repromodegroup = gui.Group(name='repromodes',value=REPROMODE)
    diffgroup = gui.Group(name='difficulty',value=DIFFICULTY)
    def modeChange(mode): # changes the MODE argument
        global MODE
        MODE = mode
        modegroup.value = mode
        app.update()
        pygame.display.flip()
    def repromodeChange(repromode):
        global REPROMODE
        REPROMODE = repromode
        repromodegroup.value = repromode
        app.update()
        pygame.display.flip()
    def difficultyChange(difficulty):
        global DIFFICULTY
        DIFFICULTY = difficulty
        diffgroup.value = difficulty
        app.update()
        pygame.display.flip()
    modeLabel = gui.Label('Select the game speed you wish to use')
    modeLabel.set_font(ThirtyTwoFont)
    modeLabelSF = gui.Label('Superfast Mode')
    modeLabelSF.set_font(ThirtyTwoFont)
    modeSuper = gui.Radio(modegroup,'superfast')
    modeSuper.connect(gui.CLICK,lambda: modeChange('superfast'))
    modeLabelF = gui.Label('Fast Mode')
    modeLabelF.set_font(ThirtyTwoFont)
    modeFast = gui.Radio(modegroup,'fast')
    modeFast.connect(gui.CLICK,lambda: modeChange('fast'))
    modeLabelS = gui.Label('Slow Mode')
    modeLabelS.set_font(ThirtyTwoFont)
    modeSlow = gui.Radio(modegroup,'slow')
    modeSlow.connect(gui.CLICK,lambda: modeChange('slow'))
    modeLabel2 = gui.Label('Select the reproductive mode you wish to use')
    modeLabel2.set_font(ThirtyTwoFont)
    modeLabelR = gui.Label('Random Mode')
    modeLabelR.set_font(ThirtyTwoFont)
    modeRand = gui.Radio(repromodegroup,'rand')
    modeRand.connect(gui.CLICK,lambda: repromodeChange('rand'))
    modeLabelE = gui.Label('Even Mode')
    modeLabelE.set_font(ThirtyTwoFont)
    modeEven = gui.Radio(repromodegroup,'even')
    modeEven.connect(gui.CLICK,lambda: repromodeChange('even'))
    modeLabel3 = gui.Label('Select the game difficulty you wish to use')
    modeLabel3.set_font(ThirtyTwoFont)
    modeLabelP = gui.Label('Pathetic')
    modeLabelP.set_font(ThirtyTwoFont)
    modePathetic = gui.Radio(diffgroup,'pathetic')
    modePathetic.connect(gui.CLICK,lambda: difficultyChange('pathetic'))
    modeLabelEs = gui.Label('Easy')
    modeLabelEs.set_font(ThirtyTwoFont)
    modeEasy = gui.Radio(diffgroup,'easy')
    modeEasy.connect(gui.CLICK,lambda: difficultyChange('easy'))
    modeLabelNorm = gui.Label('Normal')
    modeLabelNorm.set_font(ThirtyTwoFont)
    modeNorm = gui.Radio(diffgroup,'normal')
    modeNorm.connect(gui.CLICK,lambda: difficultyChange('normal'))
    modeLabelH = gui.Label('Hard')
    modeLabelH.set_font(ThirtyTwoFont)
    modeHard = gui.Radio(diffgroup,'hard')
    modeHard.connect(gui.CLICK,lambda: difficultyChange('hard'))
    modeLabelED = gui.Label('Everyone Dies')
    modeLabelED.set_font(ThirtyTwoFont)
    modeED = gui.Radio(diffgroup,'everyonedies')
    modeED.connect(gui.CLICK,lambda: difficultyChange('everyonedies'))
    
    modeTable = gui.Table()
    table.td(modeLabel,0,0,background=GRAY)
    modeTable.td(modeLabelSF,0,0,background=GRAY)
    modeTable.td(modeSuper,0,1,background=GRAY)
    modeTable.td(gui.Spacer(width=10,height=10),1,0,background=GRAY)
    modeTable.td(modeLabelF,2,0,background=GRAY)
    modeTable.td(modeFast,2,1,background=GRAY)
    modeTable.td(gui.Spacer(width=10,height=10),3,0,background=GRAY)
    modeTable.td(modeLabelS,4,0,background=GRAY)
    modeTable.td(modeSlow,4,1,background=GRAY)
    table.td(modeTable,0,1,background=GRAY)
    table.td(modeLabel2,0,2,background=GRAY)
    repromodeTable = gui.Table()
    repromodeTable.td(modeLabelR,0,0,background=GRAY)
    repromodeTable.td(modeRand,0,1,background=GRAY)
    repromodeTable.td(gui.Spacer(width=10,height=10),1,0,background=GRAY)
    repromodeTable.td(modeLabelE,2,0,background=GRAY)
    repromodeTable.td(modeEven,2,1,background=GRAY)
    table.td(repromodeTable,0,3,background=GRAY)
    diffTable = gui.Table()
    table.td(modeLabel3,0,4,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),0,0,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),0,1,background=GRAY)
    diffTable.td(modeLabelP,1,0,background=GRAY)
    diffTable.td(modePathetic,1,1,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),2,0,background=GRAY)
    diffTable.td(modeLabelEs,3,0,background=GRAY)
    diffTable.td(modeEasy,3,1,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),4,0,background=GRAY)
    diffTable.td(modeLabelNorm,5,0,background=GRAY)
    diffTable.td(modeNorm,5,1,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),6,0,background=GRAY)
    diffTable.td(modeLabelH,7,0,background=GRAY)
    diffTable.td(modeHard,7,1,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),8,0,background=GRAY)
    diffTable.td(modeLabelED,9,0,background=GRAY)
    diffTable.td(modeED,9,1,background=GRAY)
    diffTable.td(gui.Spacer(width=10,height=10),10,0,background=GRAY)
    table.td(diffTable,0,5,background=GRAY)
    
    OK = gui.Button('Restart with new settings',height=40,font=ThirtyTwoFont)
    OK.connect(gui.CLICK,restartFunc)
    table.add(OK,0,8)
    Cancel = gui.Button('Cancel',height=40,font=ThirtyTwoFont)
    Cancel.connect(gui.CLICK,runNow)
    table.add(Cancel,0,9)
    app.init(table)
    app.paint()
    pygame.display.flip()

def overviewFunc(repro=False,showmuts=False):
    global run,LOCKESCAPE
    clear_all_widgets()
    column = 0
    dictlist = {}
    for crit in allcritterlist:
        name = gui.Label(str(crit.name))
        name.set_font(overviewFontBold)
        # checks if dead
        if crit.alive == False: # crit is dead
            dead = gui.Label('DEAD',color=RED)
            dead.set_font(overviewFontBold)
            labelist = [name,dead]
        else: # crit is alive
            fgcolor = BLACK
            if crit.energy < crit.foodNeed:
                fgcolor = RED
            elif crit.energy < crit.reproCost:
                fgcolor = YELLOW
            elif crit.energy > (crit.reproCost+crit.foodNeed):
                fgcolor = GREEN
            energy = gui.Label('Energy: '+str(crit.energy),color=fgcolor)
            energy.set_font(ThirtySixFont)
            labelist = [name,energy]
            attrslist = ['max_speed','accel','maneuver','fight','strength',
                         'hide','vision']
            strlist = ['Maximum speed: ','Acceleration: ','Turn rate: ',
                       'Fighting strength: ','Carrying limit: ','Camouflage: ',
                       'Vision range: ',]
            base_attrslist = ['base_max_speed','base_accel','base_maneuver',
                              'base_fight','base_strength','hide','vision']
            step = 0
            for attr in attrslist:
                fgcolor = BLACK
                avg = EvoFunctions.critAvg(attr,critterlist)
                value = crit.__dict__[attr]
                basevalue = crit.__dict__[base_attrslist[step]]
                if value < avg:
                    fgcolor = YELLOW
                elif value > avg:
                    fgcolor = GREEN
                if value < basevalue:
                    fgcolor = RED
                text = gui.Label(strlist[step]+str(round(value,1)),color=fgcolor)
                text.set_font(ThirtySixFont)
                labelist.append(text)
                step+=1
            costTag = gui.Label('Costs')
            costTag.set_font(overviewFont)
            labelist.append(costTag)
            attrslist = ['reproCost','foodNeed',]
            strlist = ['Reproductive: ','Metabolic: ']
            step = 0
            for attr in attrslist:
                fgcolor = BLACK
                avg = EvoFunctions.critAvg(attr,critterlist)
                value = crit.__dict__[attr]
                low = avg*.75
                high = avg*1.25
                if value < low:
                    fgcolor = GREEN
                elif value > high:
                    fgcolor = RED
                text = gui.Label(strlist[step]+str(round(value,1)),color=fgcolor)
                text.set_font(ThirtySixFont)
                labelist.append(text)
                step+=1
            if showmuts == True:
                text = gui.Label('Descended from',color=BLUE)
                text.set_font(ThirtySixFont)
                labelist.append(text)
                text = gui.Label(crit.ancestor,color=BLUE)
                text.set_font(ThirtySixFont)
                labelist.append(text)
                for mut in crit.mutations:
                    text = gui.Label(str(mut),color=BLUE)
                    text.set_font(ThirtySixFont)
                    labelist.append(text)
            if repro == True and crit.energy >= crit.reproCost and len(allcritterlist) < 4:
                reproButton = gui.Button('Reproduce',font=ThirtyTwoFont)
                if column == 0:
                    reproButton.connect(gui.CLICK,lambda: reproduce(0))
                elif column == 1:
                    reproButton.connect(gui.CLICK,lambda: reproduce(1))
                elif column == 2:
                    reproButton.connect(gui.CLICK,lambda: reproduce(2))
                elif column == 3:
                    reproButton.connect(gui.CLICK,lambda: reproduce(3))
                labelist.append(reproButton)
        dictlist[column] = labelist
        column+=1
    table = gui.Table(background=GRAY,align=-1,valign=-1)
    for column in dictlist:
        frame = VertFrame(spacer=True)
        temptable = gui.Table()
        step = 0
        for widget in dictlist[column]:
            temptable.add(widget,0,step)
            step+=1
        frame.add(temptable)
        table.add(frame.container,column,0)
    continueButton = gui.Button('Continue',height=40,font=ThirtySixFont)
    if repro == False:
        continueButton.connect(gui.CLICK,runNow)
    elif repro == True:
        continueButton.connect(gui.CLICK,makeMap)
    speciesButton = gui.Button('Overview by Species',height=40,font=ThirtySixFont)
    speciesButton.connect(gui.CLICK,lambda: overviewSpecies(repro=repro))
    table.add(continueButton,2,3)
    table.add(speciesButton,3,4)
    if repro == True:
        LOCKESCAPE = True
    overviewWindow.image.fill(GRAY)
    SCREEN.blit(overviewWindow.image,(0,0))
    app.init(table)
    app.paint()
    pygame.display.flip()
    if len(CritterSprite.starved) > 0 and MODE != 'fast':
        makeDeadMessage('starve',CritterSprite.starved)
        CritterSprite.starved = []
    run = False # if someone gets here at the end of the turn run will be True

# switches to an overview window by species
def overviewSpecies(repro=False):
    global run,LOCKESCAPE
    clear_all_widgets()
    # make upper labels
    foodlabel = gui.Label('Live Food')
    foodlabel.set_font(overviewFont)
    foodimage = gui.Image(livefoodpic)

    critterlabel = gui.Label('Critters')
    critterlabel.set_font(overviewFont)
    critterimage = gui.Image(critterpic)

    predlabel = gui.Label('Predators')
    predlabel.set_font(overviewFont)
    predimage = gui.Image(enemypic)

    kleptolabel = gui.Label('Kleptoparasites')
    kleptolabel.set_font(overviewFont)
    kleptoimage = gui.Image(thiefpic)
    # fill in stats
    dictlist = {0:[foodlabel,foodimage],1:[critterlabel,critterimage],2:[predlabel,predimage],3:[kleptolabel,kleptoimage],}
    column = 0
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,'hide':0}
    foodavgs = EvoFunctions.otherAvg(LIVEFOODLIST,averages)
    textlist = ['Maximum speed: '+str(foodavgs['max_speed']),
                'Acceleration: '+str(foodavgs['accel']),
                'Maneuver: '+str(foodavgs['maneuver']),
                'Fighting strength: '+str(foodavgs['fight']),
                'Vision: '+str(foodavgs['vision']),
                'Camouflage: '+str(foodavgs['hide']),
                'Behavior: ',
                EvoFunctions.behaviorAvg(LIVEFOODLIST)]
    for text in textlist:
        label = gui.Label(text)
        label.set_font(ThirtySixFont)
        dictlist[column].append(label)

    column = 1
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,'hide':0}
    critteravgs = EvoFunctions.otherAvg(critterlist,averages)
    textlist = ['Maximum speed: '+str(critteravgs['max_speed']),
                'Acceleration: '+str(critteravgs['accel']),
                'Maneuver: '+str(critteravgs['maneuver']),
                'Fighting strength: '+str(critteravgs['fight']),
                'Vision: '+str(critteravgs['vision']),
                'Camouflage: '+str(critteravgs['hide'])]
    for text in textlist:
        label = gui.Label(text)
        label.set_font(ThirtySixFont)
        dictlist[column].append(label)

    column = 2
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,}
    predavgs = EvoFunctions.otherAvg(PREDATORLIST,averages)
    textlist = ['Maximum speed: '+str(predavgs['max_speed']),
                'Acceleration: '+str(predavgs['accel']),
                'Maneuver: '+str(predavgs['maneuver']),
                'Fighting strength: '+str(predavgs['fight']),
                'Vision: '+str(predavgs['vision']),]
    for text in textlist:
        label = gui.Label(text)
        label.set_font(ThirtySixFont)
        dictlist[column].append(label)

    column = 3
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,}
    kleptoavgs = EvoFunctions.otherAvg(THIEFLIST,averages)
    textlist = ['Maximum speed: '+str(kleptoavgs['max_speed']),
                'Acceleration: '+str(kleptoavgs['accel']),
                'Maneuver: '+str(kleptoavgs['maneuver']),
                'Fighting strength: '+str(kleptoavgs['fight']),
                'Vision: '+str(kleptoavgs['vision']),]
    for text in textlist:
        label = gui.Label(text)
        label.set_font(ThirtySixFont)
        dictlist[column].append(label)
    
    table = gui.Table(background=GRAY,align=-1,valign=-1)
    for column in dictlist:
        temptable = gui.Table(valign=-1)
        frame = gui.Container(valign=-1)
        step = 0
        for widget in dictlist[column]:
            temptable.add(widget,0,step)
            if step == 1:
                temptable.add(gui.Spacer(width=10,height=70),1,step)
            step+=1
        frame.add(temptable,0,0)
        table.add(frame,column,0)
    continueButton = gui.Button('Continue',height=40,font=ThirtySixFont)
    if repro == False:
        continueButton.connect(gui.CLICK,runNow)
    elif repro == True:
        continueButton.connect(gui.CLICK,makeMap)
    table.add(continueButton,3,step)
    if MODE != 'superfast':
        critterButton = gui.Button('Overview by Individual',height=40,font=ThirtySixFont)
        critterButton.connect(gui.CLICK,lambda: overviewFunc(repro=repro))
        table.add(critterButton,4,step+1)
    SCREEN.blit(overviewWindow.image,(0,0))
    app.init(table)
    app.paint()
    pygame.display.flip()

# manages fights
def fight(critter,enemy):
    global PLAYERALIVE,POINTS,PREDOS,THIEFOS
    doublehitsound.play()
    enemy.hit = True
    enemy.shouldAccel = 0
    pFight = critter.fight+random.randint(0,5)
    eFight = enemy.fight
    ID = enemy.name
    color = RED
    pInjure = False
    eInjure = False
    # fights for predators
    if ID == 'predator':
        if pFight > eFight:
            color = GREEN
            diff = pFight - eFight
            if diff < 2:
                message = EscapeHit
            elif diff < 4:
                message = ReverseInjury
                eInjure = True
                enemy.points-=1
                if enemy.points < 0:
                    enemy.points = 0
            else:
                message = ReverseInjury
                enemy.kill()
                PREDOS-=1
        elif eFight > pFight:
            diff = eFight - pFight
            if diff < 2:
                color = GREEN
                message = EscapeHit
            elif diff < 4:
                message = InjuryHit
                pInjure = True
                enemy.points+=1
                enemy.energyCounter+=2000
                enemy.liveCounter+=1
            else:
                message = KillHit
                critter.speed = 0
                critter.max_speed = 0
                critter.vision = 0
                critter.die()
                PLAYERALIVE = time.clock()
                if MODE == 'slow':
                    POINTS-=1
                enemy.points+=3
                enemy.energyCounter+=10000
                enemy.liveCounter+=3
        else:
            color = GREEN
            message = EscapeHit
    # fights for thieves
    elif ID == 'thief':
        if pFight > eFight:
            color = GREEN
            diff = pFight - eFight
            if diff < 2:
                message = EscapeHit
            elif diff < 4:
                message = ReverseInjury
                eInjure = True
            else:
                message = ReverseInjury
                enemy.kill()
                THIEFOS-=1
        elif eFight > pFight and len(critter.carry) > 0:
            diff = eFight - pFight
            if diff < 2:
                color = GREEN
                message = EscapeHit
            elif diff < 6:
                color = RED
                message = StealHit
                EvoFunctions.steal(critter,enemy)
                randnum = random.randint(10,15)
                for num in range(0,randnum):
                    distance = random.randint(50,150)*[-1,1][random.randint(0,1)]
                    angle = random.randint(0,359)
                    rad = angle*math.pi/180
                    newx = critter.x+(distance*math.sin(rad))
                    newy = critter.y+(distance*math.cos(rad))
                    extraSplatter = BloodTrailSprite(newx,newy,foodsplatterpic)
                    AllSprites.add(extraSplatter)
                    bloodtrails.add(extraSplatter)
            else:
                message = InjuryHit
                pInjure = True
                EvoFunctions.steal(critter,enemy)
                randnum = random.randint(10,15)
                for num in range(0,randnum):
                    distance = random.randint(50,150)*[-1,1][random.randint(0,1)]
                    angle = random.randint(0,359)
                    rad = angle*math.pi/180
                    newx = critter.x+(distance*math.sin(rad))
                    newy = critter.y+(distance*math.cos(rad))
                    extraSplatter = BloodTrailSprite(newx,newy,foodsplatterpic)
                    AllSprites.add(extraSplatter)
                    bloodtrails.add(extraSplatter)
        else:
            color = GREEN
            message = EscapeHit
    # fights for live food
    elif ID == 'live':
        if pFight > eFight:
            color = GREEN
            message = LiveFoodHit
        elif eFight > pFight:
            diff = eFight - pFight
            color = RED
            if diff < 2:
                message = FailureHit
            elif diff < 4:
                message = InjuryHit
                pInjure = True
            else:
                message = KillHit
                critter.speed = 0
                critter.max_speed = 0
                critter.vision = 0
                critter.die()
                PLAYERALIVE = time.clock()
                if MODE != 'fast':
                    POINTS-=1
        else:
            color = RED
            message = FailureHit
    if pInjure == True:
        critter.bleeding = time.time()
        critter.initialBleeding = time.time()
        decreaselist = [random.randint(20,80)/100.0,random.randint(20,80)/100.0]
        chooserlist = [random.randint(0,4),random.randint(0,4)]
        for x in range(0,len(chooserlist)):
            chooser = chooserlist[x]
            decrease = decreaselist[x]
            if chooser == 0:
                critter.accel-=round(critter.accel*decrease,1)
                if critter.accel < 0:
                    critter.accel = 0.1
            elif chooser == 1:
                critter.maneuver-=round(decrease*critter.maneuver,1)
                if critter.maneuver < 0:
                    critter.maneuver = 0.1
            elif chooser == 2:
                critter.base_speed_no_carry-=round(decrease*critter.base_speed_no_carry,1)
                if critter.base_speed_no_carry < 0:
                    critter.base_speed_no_carry = 0
                EvoFunctions.updateCarry(critter)
            elif chooser == 3:
                critter.fight-=round(decrease*critter.fight,1)
                if critter.fight < 0:
                    critter.fight = 0
            elif chooser == 4:
                critter.strength-=int(critter.strength*decrease)
                if critter.strength < 0:
                    critter.strength = 0
                EvoFunctions.updateCarry(critter)
        EvoFunctions.UpdateControlBar = True
    if eInjure == True:
        enemy.bleeding = time.time()
        enemy.initialBleeding = time.time()
        decreaselist = [random.randint(20,80)/100.0,random.randint(20,80)/100.0]
        chooserlist = [random.randint(0,3),random.randint(0,3)]
        for x in range(0,len(chooserlist)):
            chooser = chooserlist[x]
            decrease = decreaselist[x]
            if chooser == 0:
                enemy.accel*=decrease
                if enemy.accel < 0:
                    enemy.accel = 0
            elif chooser == 1:
                enemy.maneuver*=decrease
                if enemy.maneuver < 0:
                    enemy.maneuver = 0
            elif chooser == 2:
                enemy.max_speed*=decrease
                if enemy.max_speed < 0:
                    enemy.max_speed = 0
            elif chooser == 3:
                enemy.fight*=decrease
                if enemy.fight < 0:
                    enemy.fight = 0
    return (message,color)
        
# reproduces the critter sent to it
def reproduce(num):
    crit = allcritterlist[num]
    crit.energy-=crit.reproCost
    (BMS,BM,BA,BF,BS,H,V,RC,FN)=(crit.base_max_speed,crit.base_maneuver,
        crit.base_accel,crit.base_fight,crit.base_strength,
        crit.hide,crit.vision,crit.reproCost,crit.foodNeed)
    mutate(BMS,BM,BA,BF,BS,H,V,RC,FN)

# displays mutations in sequence for fast mode
def mutationSequence(mutList):
    global run,critter
    run = False
    mainFrame = VertFrame()
    for index in range(0,len(mutList)-1):
        message = mutList[index][0][1]
        genes = mutList[index][0][0]
        parent = str(mutList[index][1])
        crit = CritterSprite(genes[0],genes[1],genes[2],genes[3],genes[4],genes[5],genes[6],genes[7],genes[8])
        child = crit.name
        table = VertFrame()
        step = 0
        if message == []: # no mutation
            message = ['New critter:',child+' is descended from '+parent,'No mutations']
        else:
            message.insert(0,'Mutations:')
            message.insert(0,child+' is descended from '+parent)
            message.insert(0,'New critter:')
        for text in message:
            label = gui.Label(text)
            label.create_style()
            label.ser_font(overviewFont)
            table.add(label)
            step+=1
        mainFrame.add(table)
    superultraFrame = VertFrame()
    button = gui.Button('Sounds great!',height=40)
    button.connect(gui.CLICK,overviewFunc)
    superultraFrame.add(mainFrame.container)
    superultraFrame.add(button)
    overviewFunc(sentWidget=superultraFrame)

# mutates genes passed to it
def mutate(BMS,BM,BA,BF,BS,H,V,RC,FN):
    global run
    run = False
    (genes,message) = mutations.critterMut([BMS,BM,BA,BF,BS,H,V,RC,FN])
    CritterSprite(genes[0],genes[1],genes[2],genes[3],genes[4],genes[5],genes[6],genes[7],genes[8])
    if message != []:
        button = gui.Button('Sounds great!',height=40)
        button.connect(gui.CLICK,lambda: overviewFunc(repro=True))
        table = gui.Table()
        step = 0
        for text in message:
            label = gui.Label(text)
            label.set_font(overviewFont)
            table.add(label,0,step)
            step+=1
        table.add(button,0,step)
        app.init(table)
        app.paint()
        pygame.display.flip()
    else:
        overviewFunc(repro=True)

# creates/updates fog of war
def create_fog(critter):
    global fogMap,fog
    fog.fill((0,0,0))
    pygame.draw.circle(fog,WHITE,(critter.rect.centerx,critter.rect.centery),critter.vision)
    pygame.draw.circle(fogMap,(WHITE),(int(critter.x+10),int(critter.y+10)),critter.vision)
    return fog

# creates/updates controlbar text
def controlbartext():
    global LabelList,pointsLabel,turnLabel,nameLabel,foodLabel,ENERGYBAR,COUNTERBAR
    global energyLabel,maxspeedLabel,accelLabel,turnLabel,fightLabel,hideLabel,offspringLabeltext
    global costLabeltext
    if len(LabelList) <= 0:
        LabelList = []
        x = screenX+20
        possible = 2+(4*(TURN-2))
        if possible < 0:
            possible = 0
        pointsLabel = TextSprite(str(POINTS)+' points ('+str(possible)+' possible)',BLACK,x,10)
        textGroup.add(pointsLabel)
        LabelList.append(pointsLabel)
        turnLabel = TextSprite('Turn '+str(TURN),BLACK,x,50)
        textGroup.add(turnLabel)
        LabelList.append(turnLabel)
        nameStr = TextSprite('Critter:',BLACK,x,100)
        textGroup.add(nameStr)
        LabelList.append(nameStr)
        nameLabel = TextSprite(str(critter.name),BLACK,x,140)
        textGroup.add(nameLabel)
        LabelList.append(nameLabel)
        foodLabel = TextSprite('Carrying '+str(len(critter.carry))+'/'+str(critter.strength)
                               +' food',
                               GREEN,x,180)
        textGroup.add(foodLabel)
        LabelList.append(foodLabel)
        energyLabel = TextSprite(str(round(critter.energy,1))+' Energy',BLACK,x,220)
        textGroup.add(energyLabel)
        LabelList.append(energyLabel)
        maxspeedLabel = TextSprite(str(critter.max_speed)+' Max Speed',BLACK,x,260)
        textGroup.add(maxspeedLabel)
        LabelList.append(maxspeedLabel)
        accelLabel = TextSprite(str(critter.accel)+' Acceleration',BLACK,x,300)
        textGroup.add(accelLabel)
        LabelList.append(accelLabel)
        turnLabel = TextSprite(str(critter.maneuver)+' Turn Speed',BLACK,x,340)
        textGroup.add(turnLabel)
        LabelList.append(turnLabel)
        fightLabel = TextSprite(str(critter.fight)+' Fighting Strength',BLACK,x,380)
        textGroup.add(fightLabel)
        LabelList.append(fightLabel)
        hideLabel = TextSprite(str(critter.hide)+' Camouflage',BLACK,x,420)
        textGroup.add(hideLabel)
        LabelList.append(hideLabel)
        if MODE == 'fast':
            offspringLabel = TextSprite('Possible offspring:',BLACK,x,500)
            textGroup.add(offspringLabel)
            LabelList.append(offspringLabel)
            offspringLabeltext = TextSprite('    '+calcOffspring(critter),BLACK,x,540)
            textGroup.add(offspringLabeltext)
            LabelList.append(offspringLabeltext)
        if MODE == 'superfast':
            ENERGYBAR = Progressbar(100,GREEN,screenX/2,10)
            textGroup.add(ENERGYBAR)
            progressbars.add(ENERGYBAR)
            COUNTERBAR = Progressbar(0,MAGENTA,10,160,vertical=True)
            textGroup.add(COUNTERBAR)
            progressbars.add(COUNTERBAR)
            offspringLabel = TextSprite('Extra critters:',BLACK,x,500)
            textGroup.add(offspringLabel)
            LabelList.append(offspringLabel)
            offspringLabeltext = TextSprite('    '+str(len(allcritterlist)-1),BLACK,x,540)
            textGroup.add(offspringLabeltext)
            LabelList.append(offspringLabeltext)
            costLabel = TextSprite('Metabolic/Repro:',BLACK,x,580)
            textGroup.add(costLabel)
            LabelList.append(costLabel)
            costLabeltext = TextSprite(str(round(critter.foodNeed,1))+'/'+str(round(critter.reproCost,1)),BLACK,x,620)
            textGroup.add(costLabeltext)
            LabelList.append(costLabeltext)
        EvoFunctions.UpdateControlBar = True
    if EvoFunctions.UpdateControlBar == False:
        pass
    else:
        EvoFunctions.UpdateControlBar = False
        possible = 2+(4*(TURN-2))
        if possible < 0:
            possible = 0
        pointsLabel.update(text=(str(POINTS)+' points ('+str(possible)+' possible)'))
        turnLabel.update(text=('Turn '+str(TURN)))
        nameLabel.update(text=(str(critter.name)))
        foodLabel.update(text=('Carrying '+str(len(critter.carry))+'/'+str(int(critter.strength))
                               +' food'))
        if len(critter.carry) > critter.strength:
            foodLabel.update(color=RED)
        else:
            foodLabel.update(color=GREEN)
        energyLabel.update(text=(str(round(critter.energy,1))+' Energy'))
        maxspeedLabel.update(text=(str(round(critter.max_speed,1))+' Max Speed'))
        if critter.max_speed < critter.base_max_speed:
            maxspeedLabel.update(color=RED)
        else:
            maxspeedLabel.update(color=BLACK)
        accelLabel.update(text=(str(round(critter.accel,1))+' Acceleration'))
        if critter.accel < critter.base_accel:
            accelLabel.update(color=RED)
        else:
            accelLabel.update(color=BLACK)
        turnLabel.update(text=(str(round(critter.maneuver,1))+' Turn Speed'))
        if critter.maneuver < critter.base_maneuver:
            turnLabel.update(color=RED)
        else:
            turnLabel.update(color=BLACK)
        fightLabel.update(text=(str(round(critter.fight,1))+' Fighting Strength'))
        if critter.fight < critter.base_fight:
            fightLabel.update(color=RED)
        else:
            fightLabel.update(color=BLACK)
        hideLabel.update(text=(str(critter.hide)+' Camouflage'))
        if MODE == 'fast':
            offspring = calcOffspring(critter)
            offspringLabeltext.update(text='    '+offspring)
            if offspring != 'None':
                offspringLabeltext.update(color=GREEN)
            else:
                offspringLabeltext.update(color=BLACK)
        if MODE == 'superfast':
            upper = float(critter.base_strength*1.5)
            if upper < 1:
                upper = 1
            percent = (critter.energy/upper)*100
            if percent > 100:
                percent = 100
            if (critter.foodNeed*2) > critter.energy > (critter.foodNeed):
                color = YELLOW
            elif critter.energy < critter.foodNeed:
                color = RED
            else:
                color = GREEN
            ENERGYBAR.update(percent,color)
            offspring = str(len(allcritterlist)-1)
            offspringLabeltext.update(text='    '+offspring)
            offspringLabeltext.update(color=BLACK)
            costLabeltext.update(text=str(round(critter.foodNeed,1))+'/'+str(round(critter.reproCost,1)))

# calculate how many offspring a critter could have with its energy
def calcOffspring(critter):
    need = critter.foodNeed
    (MS,BMS,F,BF,A,BA,M,BM,S,BS) = (critter.max_speed,
        critter.base_speed_no_carry,critter.fight,critter.base_fight,
        critter.accel,critter.base_accel,critter.maneuver,
        critter.base_maneuver,critter.strength,critter.base_strength)
    while MS < BMS:
        MS+=BMS/2.
        need+=critter.foodNeed/2.0
    while F < BF:
        F+=BF/2.
        need+=critter.foodNeed/2.0
    while A < BA:
        A+=BA/2.
        need+=critter.foodNeed/2.0
    while M < BM:
        M+=BM/2.
        need+=critter.foodNeed/2.0
    while S < BS:
        S+=BS/2.
        need+=critter.foodNeed/2.0
    if need < critter.energy:
        energy = critter.energy-need
        offspring = 0
        while energy > 0:
            if energy >= critter.reproCost:
                offspring += 1
            energy-=critter.reproCost
    else:
        offspring = 'None'
    if offspring == 0:
        offspring = 'None'
    return str(offspring)

# makes a message for when one critter dies
def makeDeadMessage(deathType,names=[]):
    global DeadMessage,run,RETURNEVENT
    label = InsultOTron(deathType,48,names).labelTable
    button = gui.Button('Continue',height=30,font=ThirtyTwoFont)
    # this next line is the one that causes problems if we outsource it to another module
    if deathType == 'kill':
        button.connect(gui.CLICK,critterNext)
        RETURNEVENT = critterNext
    elif deathType == 'starve' and MODE != 'superfast':
        button.connect(gui.CLICK,lambda: overviewFunc(repro=True))
        RETURNEVENT = lambda: overviewFunc(repro=True)
    elif deathType == 'starve' and MODE == 'superfast':
        button.connect(gui.CLICK,lambda: runMessage(critter,playerSprite,foods,None,allOtherSprites))
        RETURNEVENT = lambda: runMessage(critter,playerSprite,foods,None,allOtherSprites)
    DeadMessage = VertFrame(spacer=True)
    DeadMessage.add(label)
    DeadMessage.add(button)
    app.init(DeadMessage.container)
    app.paint()
    pygame.display.flip()

def restartFunc():
    global run
    clear_all_widgets()
    critter.superkill()
    run = True
    for crit in critterlist:
        crit.superkill()
    for dead in deadlist:
        dead.superkill()
    main()

def helpFunc():
    clear_all_widgets()
    fogMap = pygame.Surface((screenX,screenY))
    fogMap.set_colorkey(WHITE)
    fog = pygame.Surface((screenX,screenY))
    fog.set_colorkey(WHITE)
    fog.set_alpha(50)
    pygame.draw.circle(fog,WHITE,(screenX/2,screenY/2),300)
    heading = 270
    (x,y) = ((screenX/2),(screenY/2))
    path = [(x,y)]
    done = False
    while not done:
        rad = heading*math.pi/180
        x = x+(-10*math.sin(rad))
        y = y+(-10*math.cos(rad))
        path.append((int(x),int(y)))
        if heading > 360:
            heading-=360
        elif heading < 0:
            heading +360
        if 86 > heading or heading > 94:
            heading+=3.5
        if x <= 0 or y <= 0:
            done = True
    for loc in path:
        pygame.draw.circle(fogMap,(WHITE),(loc[0],loc[1]),300)
    helpsurface = pygame.Surface((screenX,screenY))
    helpsurface.fill(WHITE)
    (x,y) = ((screenX/2),(screenY/2))
    terrain1 = terrainpic
    terrain2 = terrainpic2
    helpsurface.blit(terrain1,(165,213))
    helpsurface.blit(terrain2,(45,67))
    crit = pygame.transform.rotate(critterpic,290)
    pred = pygame.transform.rotate(enemypic,170)
    thief = pygame.transform.rotate(thiefpic,260)
    helpsurface.blit(crit,(x-25,y-25))
    helpsurface.blit(pred,(x+20,y+180))
    helpsurface.blit(thief,(x+180,y-20))
    helpsurface.blit(foodpic,(300,200))
    live = pygame.transform.rotate(livefoodpic,20)
    helpsurface.blit(live,(x+200,y-200))
    helpsurface.blit(bushpic,(700,100))
    helpsurface.blit(grasspic,(237,450))
    helpsurface.blit(fogMap,(0,0))
    helpsurface.blit(fog,(0,0))
    if MODE == 'superfast':
        ENERGYBAR = Progressbar(70,GREEN,screenX/2,10)
        helpsurface.blit(ENERGYBAR.image,(screenX/2-150,0))
        COUNTERBAR = Progressbar(50,MAGENTA,10,160,vertical=True)
        helpsurface.blit(COUNTERBAR.image,(0,10))
    SCREEN.blit(helpsurface,(0,0))
    # make buttons
    frame = gui.Container(align=-1,valign=-1) # upper left of frame is upper left of screen
    helptext = gui.Label('Click on any label to get more information',color=RED,font=overviewFont)
    helptext2 = gui.Label('Hit ESCAPE to unpause the game and return to the game screen',color=RED,font=overviewFont)
    critbutton = gui.Button('Critter')
    critbutton.connect(gui.CLICK,lambda: helpDisplay('critter'))
    predbutton = gui.Button('Predator')
    predbutton.connect(gui.CLICK,lambda: helpDisplay('predator'))
    thiefbutton = gui.Button('Kleptoparasite')
    thiefbutton.connect(gui.CLICK,lambda: helpDisplay('thief'))
    foodbutton = gui.Button('Food')
    foodbutton.connect(gui.CLICK,lambda: helpDisplay('food'))
    livebutton = gui.Button('Live food')
    livebutton.connect(gui.CLICK,lambda: helpDisplay('live'))
    terrainbutton = gui.Button('Terrain')
    terrainbutton.connect(gui.CLICK,lambda: helpDisplay('terrain'))
    bushbutton = gui.Button('Bush')
    bushbutton.connect(gui.CLICK,lambda: helpDisplay('bush'))
    grassbutton = gui.Button('Grass')
    grassbutton.connect(gui.CLICK,lambda: helpDisplay('grass'))
    carrybutton = gui.Button('Carrying Capacity',width=deadArea,height=40,font=overviewFont)
    carrybutton.connect(gui.CLICK,lambda: helpDisplay('carry'))
    energy2button = gui.Button('Energy',width=deadArea,height=40,font=overviewFont)
    energy2button.connect(gui.CLICK,lambda: helpDisplay('energy'))
    speedbutton = gui.Button('Max Speed',width=deadArea,height=40,font=overviewFont)
    speedbutton.connect(gui.CLICK,lambda: helpDisplay('speed'))
    accelbutton = gui.Button('Acceleration',width=deadArea,height=40,font=overviewFont)
    accelbutton.connect(gui.CLICK,lambda: helpDisplay('accel'))
    turnbutton = gui.Button('Turn Speed',width=deadArea,height=40,font=overviewFont)
    turnbutton.connect(gui.CLICK,lambda: helpDisplay('turn'))
    fightbutton = gui.Button('Fighting Strength',width=deadArea,height=40,font=overviewFont)
    fightbutton.connect(gui.CLICK,lambda: helpDisplay('fight'))
    camobutton = gui.Button('Camouflage',width=deadArea,height=80,font=overviewFont)
    camobutton.connect(gui.CLICK,lambda: helpDisplay('camo'))
    if MODE == 'superfast':
        energybutton = gui.Button('Energy')
        energybutton.connect(gui.CLICK,lambda: helpDisplay('energy'))
        generationbutton = gui.Button('Generation Progress')
        generationbutton.connect(gui.CLICK,lambda: helpDisplay('gencounter'))
        offspringbutton = gui.Button('Extra critters',width=deadArea,height=80,font=overviewFont)
        offspringbutton.connect(gui.CLICK,lambda: helpDisplay('offspring'))
        costsbutton = gui.Button('Metabolic/Repro Costs',width=deadArea,height=80,font=ThirtySixFont)
        costsbutton.connect(gui.CLICK,lambda: helpDisplay('costs'))
        frame.add(energybutton,x-20,30)
        frame.add(generationbutton,30,150)
        frame.add(offspringbutton,screenX,500)
        frame.add(costsbutton,screenX,580)
    elif MODE == 'fast':
        offspringbutton = gui.Button('Possible Offspring',width=deadArea,height=80,font=overviewFont)
        offspringbutton.connect(gui.CLICK,lambda: helpDisplay('posoffspring'))
        frame.add(offspringbutton,screenX,500)
    
    frame.add(helptext,0,screenY-60)
    frame.add(helptext2,0,screenY-30)
    frame.add(critbutton,x-25,y+30)
    frame.add(predbutton,x,y+245)
    frame.add(thiefbutton,x+160,y+45)
    frame.add(foodbutton,280,180)
    frame.add(livebutton,x+180,y-150)
    frame.add(terrainbutton,145,193)
    frame.add(bushbutton,680,80)
    frame.add(grassbutton,217,430)
    frame.add(carrybutton,screenX,180)
    frame.add(energy2button,screenX,220)
    frame.add(speedbutton,screenX,260)
    frame.add(accelbutton,screenX,300)
    frame.add(turnbutton,screenX,340)
    frame.add(fightbutton,screenX,380)
    frame.add(camobutton,screenX,420)
    app.init(frame)
    app.paint()
    pygame.display.flip()

def helpDisplay(flavor):
    def noimage(text):
        frame = VertFrame(spacer=True)
        textlist = textwrap.wrap(text,50)
        for x in textlist:
            t = gui.Label(x)
            frame.add(t)
        b = gui.Button('Continue')
        b.connect(gui.CLICK,helpFunc)
        frame.add(b)
        app.init(frame.container)
        app.paint()
        pygame.display.flip()
    def withimage(images,text):
        frame = VertFrame(spacer=True)
        textlist = textwrap.wrap(text,50)
        for image in images:
            i = gui.Image(image)
            frame.add(i)
        for x in textlist:
            t = gui.Label(x)
            frame.add(t)
        b = gui.Button('Continue')
        b.connect(gui.CLICK,helpFunc)
        frame.add(b)
        app.init(frame.container)
        app.paint()
        pygame.display.flip()
    if flavor == 'critter':
        text = 'This is you.\nTo speed up use the up arrow.\nTo slow down use the down arrow.\nTo turn use the right and left arrows.\nESCAPE pauses and unpauses the game.  When the game is paused the main menu also appears.\nIf your critter is too badly damaged to save and you don\'t want to wait for something to come along and kill you the K key will kill your critter.'
        image = [critterpic]
        withimage(image,text)
    elif flavor == 'predator':
        text = 'This is a predator.  Predators will try to kill you.  If they fail they may injure you instead.\nEarly in the game (before mutations) predators are strong enough that they will ALWAYS injure or kill you.  They are also faster than you but you turn faster than they do.  A good strategy is to try let them get close and then turn sharply.  Once you get far enough away from a predator change direction so that it does not know where you are headed.'
        image = [enemypic]
        withimage(image,text)
    elif flavor == 'thief':
        image = [thiefpic]
        text = 'This is a kleptoparasite.  Kleptoparasites will try to steal food from you.  If they beat you badly enough when they fight you for your food you will be injured.\nEven early in the game (before mutations) kleptoparasites are not strong enough to always win a fight with you - you may successfully fight them off.  They will also ignore you if you are not carrying any food.\nKleptoparasites are also faster than you but you turn faster than they do.  A good strategy is to try let them get close and then turn sharply.  Once you get far enough away from a kleptoparasite change direction so that it does not know where you are headed.'
        withimage(image,text)
    elif flavor == 'food':
        image = [foodpic]
        text = 'This is food.  Eat it for more energy.'
        withimage(image,text)
    elif flavor == 'live':
        image = [livefoodpic]
        text = 'This is live food.  It is worth twice as much energy as regular food.  However, unlike regular food it is a creature and it can run away and fight back.\nLive food attempts to avoid being eaten one of two ways: hiding or running.\nLive food with the hiding behavior will see you coming (they can see further than you can at the start) and stop suddenly using their camouflage to hide.\nLive food with the running behavior will simply try to run away from you.\nWhen you catch live food there is a fight.  Normally you will easily overpower and eat the live food but if the right mutations line up live food can injure or kill you.'
        withimage(image,text)
    elif flavor == 'bush':
        image = [bushpic]
        text = 'Bushes slow down enemies (predators and kleptoparasites) to half their normal speed.  They do not affect you or live food.'
        withimage(image,text)
    elif flavor == 'grass':
        image = [grasspic]
        text = 'Grass hides creatures in it.  They can still be seen but you must be four times closer.  Of course, enemies must be four times closer to you to see you.\nA good strategy if you are being chased is to run into grass, change direction, and stop.  The enemy chasing you will follow on the path it last saw you moving on and often go right by you.'
        withimage(image,text)
    elif flavor == 'terrain':
        image = [terrainpic,terrainpic2]
        text = 'The brown rocks scattered around the screen are just decoration.'
        withimage(image,text)
    elif flavor == 'gencounter':
        text = 'In superfast mode there are no breaks between rounds in which critters could reproduce.  Instead, this counter counts up towards a new generation.  Each time the counter reaches the top the screen flashes and a sound is played indicating that you now have one additional offspring.  You will start playing as this offspring immediately although if the offspring dies you can pick up and keep playing as the parent.  The cost of reproduction and the amount of energy you have on hand alter how fast the generation bar fills.  More energy and a lower cost of reproduction result in a faster-filling bar.'
        noimage(text)
    elif flavor == 'energy':
        text = 'Energy is the currency of the game.  It keeps you alive, heals wounds, and produces offspring.  You get it from food and the energy counter tracks how much you have.  In superfast mode the counter along the right side of the screen is supplemented with a progress bar along the top of the screen that shows your energy level.'
        noimage(text)
    elif flavor == 'carry':
        text = 'This line, normally displayed in a "Carrying X/Y food" format, shows how many food items you are carrying and how many you can hold before they slow you down.  The amount of food you are carrying goes down under one of three circumstances: it is stolen from you by a kleptoparasite, you go to the next round (slow or fast modes, all food is eaten between rounds), or it is eaten during a round (this occurs in superfast mode).\nNormally this line is green.  If it is red you are carrying too much food.  Each food item more than your carrying limit reduces your speed to 75% of what it was previously.  (If you were carrying two extra items your speed would be 75% of 75% of your maximum speed.)'
        noimage(text)
    elif flavor == 'speed':
        text = 'Your maximum speed is exactly what it sounds like.\nIf this line is red your speed has been reduced by an injury or by carrying too much food.'
        noimage(text)
    elif flavor == 'accel':
        text = 'Acceleration is how fast you speed up and slow down.\nIf this line is red you have been injured.'
        noimage(text)
    elif flavor == 'turn':
        text = 'This is how fast you turn. Higher turn speeds mean tighter turns.  Early in the game this is your only clear advantage over your enemies (besides their incredible stupidity) and tight last-minute turns will save many a day.\nIf this line is red you have been injured.'
        noimage(text)
        noimage(textlist)
    elif flavor == 'offspring':
        text = 'This shows you how many other critters you have.  If you die you will start playing as one of these critters.  Generally speaking these critters are critters you have already played that have had offspring.'
        noimage(text)
    elif flavor == 'fight':
        text = 'Fighting strength is just what it sounds like.  A random number between 0-5 is added to your fighting strength when you fight making fights somewhat unpredictable but you can never win a fight against a creature that is 6 points stronger than you and you can never lose a fight against a creature weaker than you.\nFights may conclude in one of three ways.  One of these ways, the tie, is simple: no one wins and no one gets hurt.  The other two ways vary depending on the creatures involved but most creatures have a low-damage and a high-damage winning mode.  For instance, predators injure you in a low-damage win but kill you in a high-damage win.\nIf this line is red you have been injured.'
        noimage(text)
    elif flavor == 'camo':
        text = 'Camouflage conceals you from enemies and prey.  It does this by making you harder to detect.  For instance, a creature that could normally see you 500 pixels away might only be able to detect you once you are at least 400 pixels away.  Camouflage is linked to speed - it works best when you are still.  To use 100% of your camouflage you must be stopped entirely.  A blue percentage will show how much your camouflage is helping hide you at any given moment.  The number in black shows what the strength of your camouflage is.  Both of these numbers are important - 100% of 0 camouflage is useless whereas 10% of 20 camouflage would make you extremely hard to see.'
        noimage(text)
    elif flavor == 'costs':
        text = 'The numbers shown here in an X/Y format are the energy costs of metabolism and reproduction.  Metabolic costs determine how much energy it takes to exist and to heal.  A low metabolic cost means you will lose energy slowly and heal cheaply (although not any more quickly).  You will also stop healng if your energy drops below the metabolic cost number.  This is to prevent you from starving yourself by healing.\nReproductive costs determine how much energy it takes to reproduce.  In superfast mode this means that a lower reproductive cost makes the generation counter fill faster while a higher cost slows it down.  These numbers are based off a reproductive cost of 1 - a cost of 0.5 would cause the generation counter to fill at twice normal speed and a cost of 2 would make it fill at half normal speed.'
        noimage(text)
    elif flavor == 'posoffspring':
        text = 'This number indicates how many offspring your critter could have if it returned to the burrow now.'
        noimage(text)

def historyFunc():
    global BEGINNINGLIST
    mainframe = gui.Table(background=GRAY)
    titleframe = VertFrame(spacer=True)
    titleframe.add(gui.Label(' ',font=ThirtySixFont))
    beginframe = VertFrame(spacer=True)
    beginframe.add(gui.Label('Value at Start',font=ThirtySixFont))
    nowframe = VertFrame(spacer=True)
    nowframe.add(gui.Label('Value Currently',font=ThirtySixFont))
    percentframe = VertFrame(spacer=True)
    percentframe.add(gui.Label('Percent Change',font=ThirtySixFont))
    averages = {'max_speed':0,'accel':0,'maneuver':0,'fight':0,'vision':0,'hide':0}
    name = {'max_speed':'Max Speed','accel':'Acceleration','maneuver':'Maneuver','fight':'Fighting Strength','vision':'Vision','hide':'Camouflage'}
    nowList = EvoFunctions.otherAvg(critterlist,averages)
    for key in BEGINNINGLIST['critters']:
        titleframe.add(gui.Label(name[key],font=ThirtyTwoFont))
        beginframe.add(gui.Label(str(BEGINNINGLIST['critters'][key]),font=ThirtyTwoFont))
        nowframe.add(gui.Label(str(nowList[key]),font=ThirtyTwoFont))
        percent = ((nowList[key]-float(BEGINNINGLIST['critters'][key]))/float(BEGINNINGLIST['critters'][key]))*100
        percent = round(percent,1)
        if percent > 0:
            percentframe.add(gui.Label(str(percent)+'%',font=ThirtyTwoFont,color=GREEN))
        elif percent < 0:
            percentframe.add(gui.Label(str(percent)+'%',font=ThirtyTwoFont,color=RED))
        else:
            percentframe.add(gui.Label(str(percent)+'%',font=ThirtyTwoFont))
    mainframe.add(titleframe.container,0,0)
    mainframe.add(beginframe.container,1,0)
    mainframe.add(nowframe.container,2,0)
    mainframe.add(percentframe.container,3,0)
    continuebutton = gui.Button('Continue')
    continuebutton.connect(gui.CLICK,runFunc)
    mainframe.add(continuebutton,0,1)
    app.init(mainframe)
    app.paint()
    pygame.display.flip()

def runFunc():
    global run
    clear_all_widgets()
    run = True

# set up pause menu
def makePauseAlert():
    global pauseAlert
    clear_all_widgets()
    overview = gui.Button(gui.Label('OVERVIEW',font=SeventyTwoFont))
    if MODE == 'superfast':
        overview.connect(gui.CLICK,overviewSpecies)
    else:
        overview.connect(gui.CLICK,overviewFunc)
    resume = gui.Button(gui.Label('RESUME',font=SeventyTwoFont))
    resume.connect(gui.CLICK,runFunc)
    summary = gui.Button(gui.Label('HISTORY',font=SeventyTwoFont))
    summary.connect(gui.CLICK,historyFunc)
    helpbutton = gui.Button(gui.Label('HELP',font=SeventyTwoFont))
    helpbutton.connect(gui.CLICK,helpFunc)
    setingsbutton = gui.Button(gui.Label('SETTINGS',font=SeventyTwoFont))
    setingsbutton.connect(gui.CLICK,settingsFunc)
    restart = gui.Button(gui.Label('RESTART',font=SeventyTwoFont))
    restart.connect(gui.CLICK,restartFunc)
    quitbutton = gui.Button(gui.Label('QUIT',font=SeventyTwoFont))
    quitbutton.connect(gui.CLICK,EvoFunctions.quitFunc)
    pauseAlert = VertFrame()
    if MODE == 'superfast':
        buttonlist = (resume,overview,helpbutton,setingsbutton,restart,quitbutton)
    else:
        buttonlist = (resume,overview,summary,helpbutton,setingsbutton,restart,quitbutton)
    for button in buttonlist:
        pauseAlert.add(button)
    app.init(pauseAlert.container)
    app.paint()
    pygame.display.flip()

# sets up an alert and menu for when all of your critters die
def makeAllDeadAlert():
    global AllDeadAlert
    clear_all_widgets()
    label = InsultOTron('all',48).labelTable
    helpbutton = gui.Button('HELP',font=SeventyTwoFont)
    helpbutton.connect(gui.CLICK,helpFunc)
    restart = gui.Button('RESTART',font=SeventyTwoFont)
    restart.connect(gui.CLICK,restartFunc)
    quitbutton = gui.Button('QUIT',font=SeventyTwoFont)
    quitbutton.connect(gui.CLICK,EvoFunctions.quitFunc)
    AllDeadAlert = VertFrame(spacer=True)
    buttonlist = (helpbutton,restart,quitbutton)
    AllDeadAlert.add(label)
    for button in buttonlist:
        AllDeadAlert.add(button)
    app.init(AllDeadAlert.container)
    app.paint()
    pygame.display.flip()

def clear_all_widgets():
    for widget in app.widgets:
        app.remove(widget)

overviewsurface = pygame.Surface((int(screenX+deadArea),int(screenY)))
overviewWindow = SimpleSprite(overviewsurface)        

if __name__ == '__main__':
    main()
