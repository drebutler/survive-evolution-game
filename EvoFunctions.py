'''This module contains functions that can safely be moved out of the main function (and its loop) to compact the main file.
Copyright (C) 2013 Eric Butler'''

'''This file is part of Survive.

    Survive is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Survive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Survive.  If not, see <http://www.gnu.org/licenses/>.'''

import pygame, sys, math, random, time
from pgu import gui
from pygame.locals import *
pygame.init()

overviewFont = pygame.font.SysFont(None,42)

# these globals are here because many functions are independent from
# the main functions except for these variables
UpdateControlBar = False # this tells us to update the control text
run = True

worldX = 3000
worldY = 3000

heallist = [['base_fight','fight'],['base_accel','accel'],
            ['base_maneuver','maneuver'],['base_strength','strength']]

detectionsound = pygame.mixer.Sound('sounds/detection.wav')

# menu functions
def quitFunc(event=1):
    pygame.quit()
    sys.exit()

# averages critter attributes for overview window
def critAvg(attr,critterlist):
    avg = 0.0 # make sure this is a float
    for crit in critterlist:
        avg+=crit.__dict__[attr]
    avg=avg/len(critterlist)
    return avg

# creates a list of averages for a species
def otherAvg(otherlist,averages):
    for other in otherlist:
        for attr in averages:
            averages[attr]+=other.__dict__[attr]
    for attr in averages: # average these
        averages[attr]=round(averages[attr]/float(len(otherlist)),1)
    return averages

# averages behaviors
def behaviorAvg(specieslist):
    hidecount = 0.0
    runcount = 0.0
    for x in specieslist:
        if x.behave == 'hide':
            hidecount+=1
        elif x.behave == 'run':
            runcount+=1
    hide = str(int((hidecount/len(specieslist))*100))
    runx = str(int((runcount/len(specieslist))*100))
    string = 'Run: '+runx+'%, Hide: '+hide+'%'
    return string

# steals food from critters
def steal(critter,enemy):
    global UpdateControlBar
    enemy.energyCounter+=10000
    num = random.randint(1,len(critter.carry))
    for x in range(0,num):
        food = critter.carry.pop(random.randint(0,len(critter.carry)-1))
        critter.energy-=food.value 
    UpdateControlBar = True
    updateCarry(critter)

def eatFood(critter,food):
    global UpdateControlBar
    UpdateControlBar = True
    critter.energy+=food.value
    critter.carry.append(food)
    updateCarry(critter)
    
# updates speed based on carrying capacity
def updateCarry(critter):
    carry = len(critter.carry)
    if carry > critter.strength:
        diff = carry-critter.strength
        critter.max_speed=critter.base_speed_no_carry*(.75**diff)
    else:
        critter.max_speed=critter.base_speed_no_carry

# checks to see if subject is visible to other
def isVisible(subject,other,maxSpeed=10):
    speed = subject.speed
    if speed <= 0:
        hideadjust = subject.hide*100
    else:
        speed = subject.speed
        if speed > maxSpeed:
            speed = maxSpeed
        percent = 1-(speed/maxSpeed)
        if percent < .1:
            percent = .1
        hideadjust = subject.hide*percent*50
    oVision = other.vision-hideadjust
    if subject.hidden == True:
        oVision/=4
    if oVision < 0:
        oVision = 0
    if (oVision**2) >= (((other.x-subject.x)**2)+((other.y-subject.y)**2)):
        canSee = True
    else:
        canSee = False
    return canSee

# allows enemies to track player
def Follow(enemy,critter):
    canSee = isVisible(critter,enemy)
    if canSee == True:
        if enemy.detecting == False:
            detectionsound.play()
            enemy.detecting = True
        if enemy.name == 'thief' and len(critter.carry) < 1:
            pass
        else:
            enemy.pursue = True
            enemy.turnFlag = True
            dist = (((enemy.x-critter.x)**2)+((enemy.y-critter.y)**2))**.5
            time = (dist/enemy.max_speed)*.66
            rad = critter.heading * math.pi / 180
            x = critter.speed*math.sin(rad)
            y = critter.speed*math.cos(rad)
            enemy.turn_point = (int(critter.x+(x*time)),int(critter.y+(y*time)))
    else:
        enemy.detecting = False

# allows live food to find critters and run away or hide
def escape(critter,food):
    canSee = isVisible(critter,food)
    if canSee == True:
        food.enemyPos = (critter.x,critter.y)
    else:
        food.enemyPos = False

# finds center point between two colliding objects
def findHitMarker(one,two):
    if one.x < two.x:
        x = one.x+((two.x-one.x)/2)
    elif two.x > one.x:
        x = two.x+((one.x-two.x)/2)
    else:
        x = one.x
    if two.y < one.y:
        y = two.y+((one.y-two.y)/2)
    elif one.y > two.y:
        y = one.y+((two.y-one.y)/2)
    else:
        y = one.y
    return (x,y)

# generates insults based on how you died
class InsultOTron():
    killInsults = [['Dodge faster, ','NAME'],
                   ['You\'re dead. So sad.',],
                   ['That looks like it hurt',],
                   ['Just a reminder:','up arrow helps you run away'],
                   ['It was a mercy killing.','Trust me.'],]
    starveInsults = [['Food.  It\'s what you should','have had for dinner'],
                     ['Starvation.','I hope you don\'t','have pets'],]
    everyonedeadInsults = [['Due to your stunning incompetence','all your critters have died'],
                           ['Looks like all your critters have died.','Work on that.'],
                           ['Oh hey, you\'re dead.','I\'ll pretend to be surprised.']]
    def __init__(self,death,fontsize,names=[]):
        if death == 'all':
            insult = InsultOTron.everyonedeadInsults[random.randint(0,len(InsultOTron.everyonedeadInsults)-1)]
        elif death == 'starve':
            insult = InsultOTron.starveInsults[random.randint(0,len(InsultOTron.starveInsults)-1)]
            insult2=['R.I.P.']
            for name in names:
                insult2+=[name]
        elif death == 'kill':
            insult = InsultOTron.killInsults[random.randint(0,len(InsultOTron.killInsults)-1)]
        step = 0
        if death == 'starve':
            self.labelTable = gui.Table()
            frame1 = VertFrame()
            for x in insult:
                label = gui.Label(str(x))
                label.set_font(overviewFont)
                frame1.add(label)
            self.labelTable.add(frame1.container,0,0)
            frame2 = VertFrame()
            for x in insult2:
                label = gui.Label(str(x))
                label.set_font(overviewFont)
                frame2.add(label)
            self.labelTable.add(frame2.container,0,1)
        else:
            self.labelTable = gui.Table()
            for x in insult:
                if x != 'NAME': # name is a placeholder
                    try:
                        if insult[step+1] == 'NAME': # if name is next
                            x+=names[0]
                    except IndexError:
                        pass
                    label = gui.Label(str(x))
                    label.set_font(overviewFont)
                    self.labelTable.add(label,0,step)
                    step+=1

class VertFrame():
    def __init__(self,spacer=False):
        self.container = gui.Table()
        self.subcontainer = gui.Table()
        if spacer == False:
            self.container.td(self.subcontainer,0,0,background=(146,146,146))
        else:
            self.container.td(gui.Spacer(width=10,height=2),0,0,background=(146,146,146))
            self.container.td(self.subcontainer,1,0,background=(146,146,146))
            self.container.td(gui.Spacer(width=10,height=2),2,0,background=(146,146,146))
        self.widgets = 0
        
    def add(self,widget):
        self.subcontainer.add(widget,0,self.widgets)
        self.widgets+=1
