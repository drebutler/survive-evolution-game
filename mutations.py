'''This module runs mutations for the game Survive.
Copyright (C) 2013 Eric Butler'''


'''This file is part of Survive.

    Survive is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Survive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Survive.  If not, see <http://www.gnu.org/licenses/>.'''

# format is text,value,attribute
# attribute codes are:
# BMS=0,BM=1,BA=2,BF=3,BS=4,H=5,V=6,RC=7,FN=8

import random

# BMS,BM,BA,BF,BS,H,V,RC,FN in that order
critterMins = [.5,1,.2,0,0,0,20,.5,.5]
critterSteps = [1,1,1,1,1,1,25,1,1]
critterDecimals = [1,1,1,0,0,1,0,1,1]
critterStrings = ['maximum speed','turn speed','acceleration',
                  'fighting strength','carrying strength','camouflage',
                  'vision','reproductive cost','metabolic cost',]
def critterMut(genes):
    message = []
    step = 0
    for x in genes:
        num = random.randint(1,100)
        if num <= 10: # 10% chance
            sign = [1,-1][random.randint(0,1)]
            if num <= 5: #5% chance
                adjust = abs(random.uniform(-1.,1.)+random.uniform(-1.,1.))
                #adjust = [1/percentnum,1*percentnum][random.randint(0,1)]
                x*=adjust
                if critterDecimals[step] == 0:
                    x=int(x)
                else:
                    x = round(x,critterDecimals[step])
                if adjust > 1:
                    adjustString = '+'+str(int(adjust*100)-100)+'%'
                else:
                    adjustString = '-'+str(100-int(adjust*100))+'%'
                message.append(adjustString+' of '+critterStrings[step])
            else: # other 5% chance
                adjust = sign*critterSteps[step]
                x+=sign*critterSteps[step]
                if adjust > 0:
                    adjustString = '+'+str(adjust)
                else:
                    adjustString = str(adjust)
                message.append(adjustString+' to '+critterStrings[step])
            if x < critterMins[step]:
                x = critterMins[step]
            genes[step] = x
        step+=1
    return (genes,message)

# S,A,M,V,F,H,B
enemyMins = [.1,.1,.1,0,1,0]
enemySteps = [1,1,1,25,1,0]
foodMins = [.1,.1,.1,0,1,0]
foodSteps = [1,1,1,25,1,1]
def otherMut(genes,name):
    step = 0
    for x in genes:
        num = random.randint(1,10)
        if num == 1: # 10% chance
            sign = [1,-1][random.randint(0,1)]
            if name == 'enemy' and step != 6:
                x+=(sign*enemySteps[step])
                if x < enemyMins[step]:
                    x = enemyMins[step]
            elif name == 'food' and step != 6:
                x+=(sign*foodSteps[step])
                if x < foodMins[step]:
                    x = foodMins[step]
            elif step == 6: # this is behavior, not an integer
                if x == 'hide': # flip behavior
                    x = 'run'
                else:
                    x = 'hide'
            genes[step] = x
        step+=1
    return (genes)

# picks food to mutate from
def pickFood(foodlist):
    if foodlist == []:
        genes = mutations.otherMut([5,1,2,-3,500,2,['run','hide'][random.randint(0,1)]],'food')
    else:
        food = foodlist[random.randint(0,len(foodlist)-1)]
        genes = (food.base_max_speed,food.base_accel,food.base_maneuver,food.vision,food.base_fight,food.hide,food.behave)
    return genes

# picks enemies to mutate from
# enemies with more points are more likely to be picked
def pickEnemy(enemylist):
    if enemylist == []:
        genes = mutations.otherMut([275,6,2,.4,15,'run'],'enemy')
    else:
        pickerList = []
        for enemy in enemylist:
            for x in range(0,enemy.points):
                pickerList.append(enemy)
        pick = pickerList[random.randint(0,len(pickerList)-1)]
        genes = (pick.base_max_speed,pick.base_accel,pick.base_maneuver,pick.vision,pick.base_fight,pick.hide,pick.behave)
    return genes
