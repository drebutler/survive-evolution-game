'''This module manages sprites for Survive.
Copyright (C) 2013 Eric Butler'''

'''This file is part of Survive.

    Survive is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Survive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Survive.  If not, see <http://www.gnu.org/licenses/>.'''

import pygame, sys, math, random, time,EvoFunctions
from pygame.locals import *
from pygame import gfxdraw
pygame.init()

controlBarFont = pygame.font.Font(None, 38)
popUpFont = pygame.font.Font(None, 36)

# these control how long a pop-up stays on the screen
# and how fast it rises
popUpTime = .7
popUpSpeed = 3

# screen has an additional gray 'dead area' that displays stats
deadArea = 310
SCREEN = pygame.display.set_mode((1200,700))#(0,0),pygame.FULLSCREEN)#
(screenX,screenY) = SCREEN.get_size()
screenX-=deadArea
pygame.display.set_caption('Survive')

# standard colors
BLACK = (0,0,0)
WHITE = (255,255,255)
GREEN = (0,255,0)
BLUE = (0,0,255)
RED = (255,0,0)
YELLOW = (255,255,0)
GRAY = (146,146,146)

critterpic = pygame.image.load('images/critter.png').convert_alpha()
foodpic = pygame.image.load('images/food.png').convert_alpha()
livefoodpic = pygame.image.load('images/livefood.png').convert_alpha()
holepic = pygame.image.load('images/hole.png').convert_alpha()
enemypic = pygame.image.load('images/enemy.png').convert_alpha()
thiefpic = pygame.image.load('images/thief.png').convert_alpha()
damagepic = pygame.image.load('images/damagehit.png').convert_alpha()
eatpic = pygame.image.load('images/foodhit.png').convert_alpha()
stealpic = pygame.image.load('images/stealhit.png').convert_alpha()
terrainpic = pygame.image.load('images/terrain.png').convert_alpha()
terrainpic2 = pygame.image.load('images/terrain2.png').convert_alpha()
bushpic = pygame.image.load('images/bushes.png').convert_alpha()
grasspic = pygame.image.load('images/grass.png').convert_alpha()
bloodpic = pygame.image.load('images/bloodsplatter.png').convert_alpha()
bloodpic2 = pygame.image.load('images/bloodsplatter2.png').convert_alpha()
bloodpic3 = pygame.image.load('images/bloodsplatter3.png').convert_alpha()
bloodpics = [bloodpic,bloodpic2,bloodpic3]
bloodtrailpic = pygame.image.load('images/bloodtrail.png').convert_alpha()
bloodtrailmedpic = pygame.image.load('images/bloodtrailmed.png').convert_alpha()
bloodtrailsmallpic = pygame.image.load('images/bloodtrailsmall.png').convert_alpha()
bloodtrailpics = [bloodtrailpic,bloodtrailmedpic,bloodtrailsmallpic]
foodsplatterpic = pygame.image.load('images/foodsplatter.png').convert_alpha()
progressbar = pygame.image.load('images/progressbar.png').convert_alpha()
verticalprogressbar = pygame.image.load('images/verticalprogressbar.png').convert_alpha()
purpleflare = pygame.image.load('images/purpleflare.png').convert_alpha()
critterstarved = pygame.image.load('images/critterstarved.png').convert_alpha()
enemystarved = pygame.image.load('images/enemystarved.png').convert_alpha()
run = True
cameraX = (EvoFunctions.worldX/2)-(screenX/2)
cameraY = (EvoFunctions.worldY/2)-(screenY/2)

# make sprite groups
foods = pygame.sprite.Group()
livefoods = pygame.sprite.Group()
playerSprite = pygame.sprite.Group()
allOtherSprites = pygame.sprite.Group()
oddTerrainSprites = pygame.sprite.Group()
hits = pygame.sprite.Group()
enemies = pygame.sprite.Group()
movers = pygame.sprite.Group()
AllSprites = pygame.sprite.Group()
popUps = pygame.sprite.Group()
textGroup = pygame.sprite.Group()
bloodtrails = pygame.sprite.Group()
progressbars = pygame.sprite.Group()
notdiscovered = pygame.sprite.Group() # for static features that have not been seen

# make a list of all sprite groups for fast clearing
SpriteGroups = [foods,playerSprite,allOtherSprites,notdiscovered,
                hits,enemies,movers,AllSprites,popUps,textGroup,
                livefoods,oddTerrainSprites,bloodtrails,progressbars]

# creates random positions at least x distance from the burrow - modified to avoid other locations
def notOnBurrow(dist,coords=(EvoFunctions.worldX,EvoFunctions.worldY)):
    counter = 0 # this prevents the function from trying to find a position impossibly far away forever
    while counter < 5:
        (x,y) = (random.randint(50,int(EvoFunctions.worldX-50)),random.randint(50,int(EvoFunctions.worldY-50)))
        (burrowX,burrowY) = (int(coords[0])/2,int(coords[1])/2)
        d = (((x-burrowX)**2)+((y-burrowY)**2))**.5
        if d > dist:
            counter == 5
        counter+=1
    return (x,y)

def TerrainPick(num):
    terrainGrid = []
    hnum = int(num**.5)
    vnum = int(num/hnum)
    for x in range(0,EvoFunctions.worldX,EvoFunctions.worldX/hnum):
        for y in range(0,EvoFunctions.worldY,EvoFunctions.worldY/vnum):
            terrainGrid.append((x,y))
    templist = []
    try:
        for x in range(0,num):
            found = True
            while found:
                pick = terrainGrid.pop(random.randint(0,len(terrainGrid)-1))
                new = (pick[0]+random.randint(0,(int((EvoFunctions.worldX/hnum)*1.2))),pick[1]+random.randint(0,(int((EvoFunctions.worldY/vnum)*1.2))))
                d = (((new[0]-(EvoFunctions.worldX/2))**2)+((new[1]-(EvoFunctions.worldY/2))**2))**.5
                if d > 50:
                    found = False
                found = False
            templist.append(new)
    except ValueError:
        pass
    return templist

# allows movers to turn away from a threat
def evade(pos,evader):
    dx = evader.x - pos[0]
    dy = evader.y - pos[1]
    rads = math.atan2(-dy,dx)
    rads %= 2*math.pi
    turnDirection = math.degrees(rads)+90 # is -90 to get heading to that point
    rad = turnDirection*math.pi/180
    evader.turn_point= (500*math.sin(rad),500*math.cos(rad))
    if evader.turn_point[0] > (EvoFunctions.worldX-50):
       evader.turn_point = (EvoFunctions.worldX-50,evader.turn_point[1])
    elif evader.turn_point[0] < 50:
        evader.turn_point = (50,evader.turn_point[1])
    if evader.turn_point[1] > (EvoFunctions.worldY-50):
       evader.turn_point = (evader.turn_point[0],EvoFunctions.worldY-50)
    elif evader.turn_point[1] < 50:
        evader.turn_point = (evader.turn_point[0],50)

class TerrainSprite(pygame.sprite.Sprite):
    def __init__(self,image=False,x=False,y=False):
        pygame.sprite.Sprite.__init__(self)
        if image == False:
            self.image = [terrainpic,terrainpic2][random.randint(0,1)]
        else:
            self.image = image
        self.radius = 30
        self.rect = self.image.get_rect()
        if x == False or y == False:
            (self.x,self.y) = notOnBurrow(50)
        else:
            (self.x,self.y) = (x,y)
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)
        
    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

class InterestingTerrainPatch():
    def __init__(self):
        terrain = InterestingTerrainSprite()
        name = terrain.name
        (x,y) = (terrain.x,terrain.y)
        allOtherSprites.add(terrain)
        oddTerrainSprites.add(terrain)
        for num in range(0,6):
            x2 = x+random.randint(-150,150)
            y2 = y+random.randint(-150,150)
            t = InterestingTerrainSprite(name,x2,y2)
            allOtherSprites.add(t)
            oddTerrainSprites.add(t)

class InterestingTerrainSprite(pygame.sprite.Sprite):
    def __init__(self,name=False,x=False,y=False):
        pygame.sprite.Sprite.__init__(self)
        if name == False:
            rand = random.randint(0,1)
        elif name == 'slowdown':
            rand = 0
        else:
            rand = 1
        if rand == 0:
            self.name = 'slowdown'
            self.image = bushpic
        elif rand == 1:
            self.name = 'hider'
            self.image =  grasspic
        self.radius = 100
        self.rect = self.image.get_rect()
        if x == False or y == False:
            (self.x,self.y) = notOnBurrow(500)
        else:
            (self.x,self.y) = (x,y)
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)
        
    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

class Progressbar(pygame.sprite.Sprite): # ProgressBar appears to be a PGU widget, use Progressbar
    def __init__(self,percent,color,x,y,vertical=False):
        pygame.sprite.Sprite.__init__(self)
        self.vertical = vertical
        if not self.vertical:
            self.image = progressbar.copy()
            self.barsurface = self.image.subsurface((2,2,296,16))
        else:
            self.image = verticalprogressbar.copy()
            self.barsurface = self.image.subsurface((2,2,16,296))
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.width = self.barsurface.get_width()
        self.height = self.image.get_height()
        self.update(percent,color)
        
    def update(self,percent,color):
        if not self.vertical:
            pixelpercent = int(percent*((self.width)/100.))
            pygame.draw.rect(self.barsurface,color,(0,0,pixelpercent,16))
            pygame.draw.rect(self.barsurface,(255,255,255),(pixelpercent,0,self.width-pixelpercent,16))
        else:
            pixelpercent = int(percent*((self.height)/100.))
            pygame.draw.rect(self.barsurface,color,(0,self.height-pixelpercent,16,self.height))
            pygame.draw.rect(self.barsurface,(255,255,255),(0,0,16,self.height-pixelpercent))
        
class HitSprite(pygame.sprite.Sprite):
    def __init__(self,image,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

class PopUpSprite(pygame.sprite.Sprite):
    def __init__(self,text,color,x,y):
        pygame.sprite.Sprite.__init__(self)
        label = popUpFont.render(str(text),0,(color))
        size = label.get_size()
        image = pygame.Surface(size)
        image.set_colorkey((0,0,0))
        image.blit(label,(0,0))
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y
        self.x = x
        self.y = y
        self.time = time.clock()
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

    def cameraCoords(self,cameraX,cameraY):
        if (time.clock() - self.time) >= popUpTime:
            self.kill()
        self.y-=popUpSpeed
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

class TextSprite(pygame.sprite.Sprite):
    def __init__(self,text,color,x,y):
        pygame.sprite.Sprite.__init__(self)
        (self.x,self.y) = (x,y)
        self.text = text
        self.color = color
        self.update(text,color)

    def update(self,text=False,color=False):
        if text != False:
            self.text = text
        if color != False:
            self.color = color
        self.image = controlBarFont.render(str(self.text),1,self.color)
        self.rect = self.image.get_rect()
        (self.rect.x,self.rect.y) = (self.x,self.y)

class BigBloodSplatter(pygame.sprite.Sprite):
    def __init__(self,x,y,randrange,distrange):
        pygame.sprite.Sprite.__init__(self)
        splatter = BloodTrailSprite(x,y,bloodpics[random.randint(0,2)])
        bloodtrails.add(splatter)
        AllSprites.add(splatter)
        for splat in range(0,random.randint(randrange[0],randrange[1])):
            distance = random.randint(distrange[0],distrange[1])*[-1,1][random.randint(0,1)]
            angle = random.randint(0,359)
            rad = angle*math.pi/180
            newx = x+(distance*math.sin(rad))
            newy = y+(distance*math.cos(rad))
            splatter = BloodTrailSprite(newx,newy,'bloodtrail')
            bloodtrails.add(splatter)
            AllSprites.add(splatter)

# for making a temporary sprite for blood trail
class BloodTrailSprite(pygame.sprite.Sprite):
    def __init__(self,x,y,image):
        pygame.sprite.Sprite.__init__(self)
        if image == 'bloodtrail':
            image = bloodtrailpics[random.randint(0,2)]
        self.image = image.copy()
        self.rect = self.image.get_rect()
        (self.x,self.y) = (x,y)
        self.name = 'blood' # need this so that methods that ID sprites can find a name
        self.counter = 300
        if image == bloodpic:
            self.counter = 400
        
    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)
        
    def draw(self):
        if self.counter < 101:
            temp = pygame.Surface((self.image.get_width(),self.image.get_height())).convert()
            temp.blit(SCREEN,(-self.rect.centerx,-self.rect.centery))
            temp.blit(self.image,(0,0))
            temp.set_alpha(255*(self.counter/100.))       
            SCREEN.blit(temp,(self.rect.centerx,self.rect.centery))
        else:
            SCREEN.blit(self.image,(self.rect.centerx,self.rect.centery))

# for making an image a sprite to be drawn in a group
class SimpleSprite(pygame.sprite.Sprite):
    def __init__(self,image):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect()

def makefoodpos():
    for x in range(0,EvoFunctions.worldX,EvoFunctions.worldX/5):
        for y in range(0,EvoFunctions.worldY,EvoFunctions.worldY/5):
            FoodSprite.unusedfoodpos.append((x,y))

class FoodSprite(pygame.sprite.Sprite):
    unusedfoodpos = []
    
    def __init__(self,dist=None,coords=None):
        pygame.sprite.Sprite.__init__(self)
        self.image = foodpic
        self.value = 1
        self.name = 'food'
        self.timeeaten = 0
        self.invincible = False # always, but we need this to use the same methods on live and not-live food
        self.radius = 30
        self.rect = self.image.get_rect()
        self.pos = None
        if coords != None:
            (self.x,self.y) = notOnBurrow(100,coords=coords)
        else:
            (self.x,self.y) = notOnBurrow(100)
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)
        
    def place(self):
        self.pos = FoodSprite.unusedfoodpos.pop(random.randint(0,len(FoodSprite.unusedfoodpos)-1))
        done = False
        while not done:
            (self.x,self.y) = (self.pos[0]+random.randint(-450,450),self.pos[1]+random.randint(-450,450))
            if (EvoFunctions.worldX-10) > self.x > 10 and (EvoFunctions.worldY-10) > self.y > 10:
                done = True
    
    def superkill(self):
        if self.pos not in FoodSprite.unusedfoodpos:
            FoodSprite.unusedfoodpos.append(self.pos)
        self.kill()

    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

# establish a class for things that move around the scree
class Mover(pygame.sprite.Sprite):
    def __init__(self,(S,A,M,V,F,H,B),dist=100,coords=None):
        pygame.sprite.Sprite.__init__(self)
        self.base_maneuver = M
        self.maneuver = M
        self.base_accel = A
        self.accel = A
        self.base_max_speed = S
        self.max_speed = S
        self.speed = random.uniform(0,self.base_max_speed)
        self.vision = V
        self.base_fight = F
        self.fight = F
        self.hide = H
        self.behave = B
        self.turn_point = (0,0)
        self.pickPoint() # resets turn point
        self.offburrow = dist # how far away from burrows is this placed?
        self.image = foodpic # need a random filler image, will replace
        self.baseimage = foodpic
        self.heading = random.randint(0,359)
        self.turnDirection = 0
        self.Turn = 0
        self.shouldAccel = 0
        self.hidden = False
        self.slowed = False
        self.bleeding = False
        self.initialBleeding = False
        self.counter = 0
        self.rect = self.baseimage.get_rect()
        self.place(coords=coords)

    def place(self,coords=None): # allows prexisting food to take a new position on a new map
        self.heading = random.randint(0,359)
        if coords == None:
            (self.x,self.y) = notOnBurrow(self.offburrow)
        else:
            (self.x,self.y) = notOnBurrow(self.offburrow,coords=coords)
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

    def walk(self):
        squaredDist = (((self.turn_point[0]-self.x)**2)+((self.turn_point[1]-self.y)**2))
        if squaredDist <= 10:
            self.pickPoint()
            self.turn()
        else:
            self.turn()

    def pickPoint(self):
        self.turn_point = (random.randint(50,EvoFunctions.worldX-50),random.randint(50,EvoFunctions.worldY-50))

    def turn(self):
        if self.turn_point != False:
            dx = self.x-self.turn_point[0]
            dy = self.y-self.turn_point[1]
            rads = math.atan2(-dy,dx)
            rads %= 2*math.pi
            self.turnDirection = math.degrees(rads)-90
            if self.turnDirection < 0:
                self.turnDirection+=360
            elif self.turnDirection > 359:
                self.turnDirection-360
            headadjust = self.turnDirection-self.heading
            if headadjust < 0:
                headadjust+=360
            elif headadjust > 359:
                headadjust-360
            if headadjust > 180:
                self.Turn = -1
            else:
                self.Turn = 1
            targetSpeed = self.max_speed/2
            radius = self.speed/(self.maneuver*0.0174532925) # convert to radians
            centerHeading = self.heading+(90*self.Turn)
            if centerHeading > 359:
                centerHeading -= 360
            elif centerHeading < 0:
                centerHeading +=360
            rad = centerHeading*math.pi/180
            center = ((self.x+(radius*math.sin(rad))),(self.y+(radius*math.cos(rad))))
            self.shouldAccel = 0
            dist = ((center[0]-self.turn_point[0])**2)+((center[1]-self.turn_point[1])**2)
            self.shouldAccel = 0
            if dist > (radius**2):
                self.shouldAccel = 1
            elif dist < (radius**2):
                self.shouldAccel = -1
        self.heading+=self.maneuver*self.Turn
        self.speed+=self.accel*self.shouldAccel
        self.checkPos()

    def checkPos(self):
        # fix headings and speeds so they don't become unallowable values
        if self.heading > 359:
            self.heading-=360
        elif self.heading < 0:
            self.heading+=360
        if self.speed > (self.max_speed/3) and self.slowed == True:
            self.counter+=1
            self.speed = (self.max_speed/3)
        elif self.speed > self.max_speed:
            self.speed = self.max_speed
        elif self.speed < 0:
            self.speed = 0
        # adjust mover speed and position
        rad = self.heading*math.pi/180
        self.x += self.speed*math.sin(rad)
        self.y += self.speed*math.cos(rad)
        # prevent mover from running off screen
        if self.x > (EvoFunctions.worldX - 50):
            self.x = (EvoFunctions.worldX - 50)
            self.hitWall = True
        elif self.x < 0:
            self.x = 0
            self.hitWall = True
        if self.y > (EvoFunctions.worldY - 50):
            self.y = (EvoFunctions.worldY - 50)
            self.hitWall = True
        elif self.y < 0:
            self.y = 0
            self.hitWall = True
        self.image = pygame.transform.rotate(self.baseimage,self.heading)

    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

class LiveFoodSprite(Mover):
    def __init__(self,(S,A,M,V,F,H,B),dist=None,coords=None):
        Mover.__init__(self,(S,A,M,V,F,H,B),dist=None,coords=None)
        self.baseimage = livefoodpic
        self.image = self.baseimage
        self.value = 2
        self.name = 'live'
        self.timeeaten = 0
        self.radius = 30
        # things calculated from environment and genes
        self.speed = random.randint(0,self.max_speed)
        self.enemyPos = False
        self.invincible = False # protects the food from the critter for a time
        self.turn_point = (random.randint(10,EvoFunctions.worldX-10),random.randint(10,EvoFunctions.worldY-10))
        self.Timer = time.clock()
        self.timeInterval = 0
        self.hitWall = False # did we just hit a wall? cleared as soon as a turn is set
        self.hitTurn = False # is the current turn due to hitting a wall?
        self.turnFlag = False
        self.rect = self.baseimage.get_rect()

    def evade(self):
        if self.behave == 'hide':
            self.speed+=self.accel*-1
            self.checkPos()
        else:
            evade(self.enemyPos,self)
            self.turn()
            
    def superkill(self): # method means something on static food, used here to allow same calls on all foods
        self.kill()

    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)
        if self.enemyPos == False:
            self.walk()
        else:
            self.evade()
        if self.invincible != False:
            if (time.clock()-self.invincible) > 1:
                self.invincible = False

class EnemySprite(Mover):
    liveCounterTime = (4,6)
    def __init__(self,name,(S,A,M,V,F,H,B),dist=None,coords=None):
        Mover.__init__(self,(S,A,M,V,F,H,B),dist=None,coords=None)
        self.name = name
        if name == 'predator':
            self.image = enemypic
            self.baseimage = enemypic
        elif name == 'thief':
            self.image = thiefpic
            self.baseimage = thiefpic
        self.rect = self.image.get_rect()
        self.radius = 30
        self.detecting = False
        self.offburrow = 400 # how far away from burrows is this placed?
        (self.x,self.y) = notOnBurrow(self.offburrow)
        # enemy attributes that can change during a turn
        self.points = 1
        self.hit = False
        self.liveCounter = random.randint(EnemySprite.liveCounterTime[0],EnemySprite.liveCounterTime[1])
        self.alive = True
        self.energyCounter = random.randint(300,500)

namelist = ['Sargon','Hammurabi','Rameses','Gilgamesh','Seti','Khufu','Ashurbanipal','Nebuchadnezzar','Menkare','Xerxes','Darius','Cyrus',
            'Mursili','Tiglathpilesar','Shalmaneser','Esarhaddon','Naram-sin','Ashurnasirpal','Hattusili',]
usednames = []
allcritterlist = []
critterlist = []
deadlist = []
class CritterSprite(pygame.sprite.Sprite):
    starved = [] # list of critters who have starved this round
    def __init__(self,BMS,BM,BA,BF,BS,H,V,RC,FN,ancestor=False,mutations=[],invincible=0):
        pygame.sprite.Sprite.__init__(self)
        self.image = critterpic
        self.baseimage = critterpic
        self.rect = self.image.get_rect()
        self.radius = 35
        self.heading = 180
        self.speed = 0
        (self.x,self.y) = (EvoFunctions.worldX/2,EvoFunctions.worldY/2)
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)
        name = namelist[random.randint(0,len(namelist)-1)]
        while name in usednames:
            name = namelist[random.randint(0,len(namelist)-1)]
        usednames.append(name)
        self.name = name
        self.alive = True
        self.usedThisRound = False
        self.hidden = False
        self.bleeding = False
        self.initialBleeding = False
        self.ancestor = ancestor
        self.mutations = mutations
        # attributes for superfast rounds only
        self.foodCounter = 0
        self.invincible = invincible
        # critter attributes that can be changed by mutations
        self.base_max_speed = BMS
        self.base_maneuver = BM
        self.base_accel = BA
        self.base_fight = BF
        self.base_strength = BS
        self.hide = H
        self.vision = V
        self.reproCost = RC
        self.foodNeed = FN
        # attributes that change during a round
        self.base_speed_no_carry = self.base_max_speed # tracks injuries separately from carrying too much
        self.max_speed = self.base_max_speed
        self.carry = []
        self.energy = 0
        self.maneuver = self.base_maneuver
        self.accel = self.base_accel
        self.strength = self.base_strength
        self.fight = self.base_fight

        critterlist.append(self)
        allcritterlist.append(self)

    def move(self,headingdelta,speeddelta):
        self.heading+=(headingdelta*self.maneuver)
        self.speed+=(speeddelta*self.accel)
        # fix headings and speeds so they don't become unallowable values
        if self.heading > 359:
            self.heading-=360
        elif self.heading < 0:
            self.heading+=360
        if self.speed < 0:
            self.speed = 0
        elif self.speed > self.max_speed:
            self.speed = self.max_speed
        # allow critter to slide along wall
        moved = False
        rad = self.heading*math.pi/180
        newx = self.x+self.speed*math.sin(rad)
        newy = self.y+self.speed*math.cos(rad)
        if newx > (EvoFunctions.worldX - 50):
            if self.heading < 90:
                self.y += self.speed
                if self.y > (EvoFunctions.worldY-50):
                    self.y = (EvoFunctions.worldY-50)
                moved = True
            elif 90 < self.heading:
                self.y -= self.speed
                if self.y < 0:
                    self.y = 0
                moved = True
        elif newx < 0:
            if 180 < self.heading < 270:
                self.y -= self.speed
                if self.y < 0:
                    self.y = 0
                moved = True
            elif 270 < self.heading:
                self.y += self.speed
                if self.y > (EvoFunctions.worldY-50):
                    self.y = (EvoFunctions.worldY-50)
                moved = True
        if newy > (EvoFunctions.worldY - 50):
            if 90 > self.heading:
                self.x += self.speed
                if self.x > (EvoFunctions.worldX-50):
                    self.x = (EvoFunctions.worldX-50)
                moved = True
            elif 270 < self.heading:
                self.x -= self.speed
                if self.x < 0:
                    self.x = 0
                moved = True
        elif newy < 0:
            if 90 < self.heading < 180:
                self.x += self.speed
                if self.x > (EvoFunctions.worldX-50):
                    self.x = (EvoFunctions.worldX-50)
                moved = True
            elif 180 < self.heading < 270:
                self.x -= self.speed
                if self.x < 0:
                    self.x = 0
                moved = True
        # if critter not on wall move it
        if moved == False:
            self.x = newx
            self.y = newy
        self.image = pygame.transform.rotate(self.baseimage,self.heading)

    def cameraCoords(self,cameraX,cameraY):
        (self.rect.centerx,self.rect.centery) = (self.x-cameraX,self.y-cameraY)

    def die(self):
        self.alive = False
        deadlist.append(self)
        self.kill()
        try:
            critterlist.remove(self)
        except ValueError:
            pass

    def superkill(self):
        try:
            allcritterlist.remove(self)
        except ValueError:
            pass
        try:
            usednames.remove(self.name)
        except ValueError:
            pass
        try:
            critterlist.remove(self)
        except ValueError:
            pass
        try:
            deadlist.remove(self)
        except ValueError:
            pass
